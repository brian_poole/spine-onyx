'use strict';

// Import Onyx
import * as Onyx from '../../lib/onyx/scripts/main.js';

// Import Error Console
import ErrorConsole from './error-console.js';
new ErrorConsole({elementId: 'error-console', alertOnError: false});

// Import Scenes
import SceneGame from './game.js';

// Init game
window.game = new SceneGame();
game.onload();
game.start();