'use strict';

import * as Onyx from '../../lib/onyx/scripts/main.js';

import ImportSpine from '../../module/import-spine';

const CHARACTERS = {
    "vine" : {
        src: "res/vine-pro.json", 
        atlas: "res/vine.atlas",
        animation: "grow"
    },
    "stretchyman" : {
        src: "res/stretchyman-pro.json", 
        atlas: "res/stretchyman.atlas",
        animation: "sneak"
    },
    "raptor" : {
        src: "res/raptor-pro.json", 
        atlas: "res/raptor.atlas",
        animation: "walk"
    },
    "spineboy" : {
        src: "res/spineboy-ess.json", 
        atlas: "res/spineboy.atlas",
        animation: "walk"
    }
}

export default class SceneGame extends Onyx.Scene {
    /**
     * @constructor
     */
    constructor(){
        super({
            elementId: "scene-game",
            clock: new Onyx.Clock()
        });

        this.characters = [];
        this.visibleCharacterIndex = 0;
    }
    
    /**
     * Load the characters into the scene
     * Called when scene is first loaded
     */
    onload(){
        super.onload();
        
        // Setup the camera and bind it to the viewport
        this.camera = new Onyx.Camera();
        this.add(this.camera);
        this.viewports[0].bindToCamera(this.camera);

        // Run through a list of the characters (as defined in the CHARACTERS constant) and import their Spine data.
        for(let name in CHARACTERS){
            let character = CHARACTERS[name];

            // Import Spine
            new ImportSpine({src: character.src, atlas: character.atlas}).then((model) => {
                // Create a new entity to use the model with and add it to the scene
                let newCharacter = new Onyx.Entity(name, model);
                this.add(newCharacter);
                this.characters.push(newCharacter);
    
                // Set the Spine animation
                model.state.setAnimation(0, character.animation, true);

                // Make them invisible, so not all of them are on the screen at once.
                newCharacter.visible = false;
                if(name === "raptor"){
                    this.cycleVisible(this.characters.length -1);
                }
            });
        }
    }

    /**
     * Keeps the camera centered
     * Called once every game frame.
     */
    update(...args){
        super.update(...args);

        // Keep camera centered
        game.camera.transform.x = -game.viewports[0].width / 2;
    }

    /** 
     * Cycles the visibility status 
     * Called when the viewport is clicked, and once when the character "raptor" is loaded
     * @param {Number} visibleIndex Index of the character to be visible
     */
    cycleVisible(visibleIndex = this.visibleCharacterIndex + 1){
        for(let character of this.characters){
            character.visible = false;
        }

        this.visibleCharacterIndex = visibleIndex;
        if(this.visibleCharacterIndex > this.characters.length - 1) this.visibleCharacterIndex = 0;
        this.characters[this.visibleCharacterIndex].visible = true;
    }
}