// This code is a modified version of Web Starter Kit's gulp script (https://github.com/google/web-starter-kit/blob/master/gulpfile.babel.js)

'use strict';

import fs from 'fs';
import path from 'path';
import gulp from 'gulp';
import gulpfile from 'gulp-file';
import gulpUtil from 'gulp-util';
import connect from 'gulp-connect';
import watch from 'gulp-watch';
import sequence from 'gulp-watch-sequence';
import htmlreplace from 'gulp-html-replace';
import del from 'del';
import runSequence from 'run-sequence';
import swPrecache from 'sw-precache';
import sourcemaps from 'gulp-sourcemaps';  
import rollup from 'rollup-stream';  
// import rollupIncludePaths from 'rollup-plugin-includepaths';
import babel from 'rollup-plugin-babel';
import gulpLoadPlugins from 'gulp-load-plugins';
import source from 'vinyl-source-stream';
import buffer from 'vinyl-buffer';
import map from 'map-stream';
import pkg from './package.json';

const $ = gulpLoadPlugins();

// For build date
var today = new Date();
var todayFormatted = today.getFullYear() + "." + (today.getMonth() + 1) + "." + today.getDate();

// Lint JavaScript
gulp.task('lint', () =>
	gulp.src('app/scripts/**/*.js')
		// .pipe($.eslint())
		// .pipe($.eslint.format())
);

// Copy all files at the root level (app)
gulp.task('copy', () =>
	gulp.src([
		'app/**/*',
		'!app/lib/**/*'
		// 'node_modules/apache-server-configs/dist/.htaccess'
	], {
		dot: true
	})
	.pipe(gulp.dest('dist'))
	.pipe($.size({ title: 'copy' }))
);

// Concatenate and minify JavaScript.
gulp.task('scripts', function() {
  return rollup({
		input: './app/scripts/main.js',
		format: 'es',
		sourcemap: true,
		name: 'OnyxSpine',
		plugins: [
			babel({ presets: ['es2015-rollup'], "plugins": ["transform-decorators-legacy"], babelrc: false })
		]
    })

    // point to the entry file.
    .pipe(source('main.js', './app/scripts'))

    // buffer the output. most gulp plugins, including gulp-sourcemaps, don't support streams.
    .pipe(buffer())

    // tell gulp-sourcemaps to load the inline sourcemap produced by rollup-stream.
    .pipe(sourcemaps.init({loadMaps: true}))

	// transform the code further here.
	.pipe($.concat('main.min.js'))
	// .pipe($.uglify({ preserveComments: 'some' }).on('error', gulpUtil.log))

    // write the sourcemap alongside the output file.
    .pipe(sourcemaps.write('.'))

    // and output to ./dist/main.js as normal.
    .pipe(gulp.dest('./dist'));
});

gulp.task('scripts-lib', () =>
	gulp.src([
		// Note: Since we are not using useref in the scripts build pipeline,
		//       you need to explicitly list your scripts here in the right order
		//       to be correctly concatenated
		'./app/lib/*.js'
	])
		.pipe(gulp.dest('.tmp/scripts'))
		.pipe($.concat('lib.min.js'))
		// Output files
		.pipe($.size({ title: 'scripts-lib' }))
		// .pipe($.sourcemaps.write('.'))
		.pipe(gulp.dest('dist/scripts'))
);

// Clean output directory
gulp.task('clean', () => del(['.tmp', 'dist/*', '!dist/.git'], { dot: true }));

// Reload
gulp.task('reload', function () {
  gulp.src('./app/*.html')
    .pipe(connect.reload());
	// connect.reload();
});

// Watch files for changes & reload
gulp.task('serve', [], () => {
	var queue = sequence(500);

	runSequence('clean', 'copy', ['lint', 'scripts'], () => {
		watch(['app/**/*.js', '../../dist/**/*.js'], { name : 'JS' }, queue.getHandler('lint', 'scripts', 'reload'));
		// watch(['app/lib/**/*.js'], { name : 'JS-Lib' }, queue.getHandler('lint', 'scripts-lib', 'reload'));
		
		connect.server({
			name: 'Development Server',
			root: ['./dist/', './'],
			port: process.env.PORT || 8080
		});
	});
});

// Build production files, the default task
gulp.task('default', ['clean'], () =>
	runSequence(
		['lint', 'scripts']
	), () => {
		console.log("Build complete.");
	}
);

// Build production files, but don't clean
gulp.task('build-noclean', ['lint', 'scripts'], () => {
	console.log("Build complete.");
});