'use strict';

import * as Onyx from '../lib/onyx/scripts/main.js';
import { spine } from '../lib/spine-ts/build/spine-core';

const QUAD_TRIANGLES = [0, 1, 2, 2, 3, 0];
const VERTEX_SIZE = 2 + 2 + 4;

export default class Spine extends Onyx.Component {
    /**
     * A Spine Animation component to be used with Onyx.Entity
     * @param {Object} Options
     * @param {Onyx.Transform} transform The transform (optional)
     * @param {Object} data JSON parsed Spine data
     * @param {spine.atlas} atlas JSON parsed atlas data
     * @param {Onyx.Texture[]} textures List of textures to be used with this Spine model
     * @param {Number} lighting The transform (optional)
     */
    constructor({transform = new Onyx.Transform(), data = {}, atlas = {}, textures = [], lighting = 0} = {}) {
        super();
        
        this.tempPos = new spine.Vector2();
		this.tempUv = new spine.Vector2();
		this.tempLight = new spine.Color();
        this.tempDark = new spine.Color();
        this.tempColor = new spine.Color();
        this.zOffset = 0.1;

        this.clipper = new spine.SkeletonClipping();

        this.transform = transform;
        this.lighting = lighting;

        var atlasLoader = new spine.AtlasAttachmentLoader(atlas);
        var skeletonJson = new spine.SkeletonJson(atlasLoader);
        skeletonJson.scale = 0.4;
        var skeletonData = skeletonJson.readSkeletonData(data);

        this.skeleton = new spine.Skeleton(skeletonData);
        let animData = new spine.AnimationStateData(skeletonData);
        this.state = new spine.AnimationState(animData);
        
        this.mesh = new Onyx.Mesh();
        this.materials = [];
        this.textures = textures;

        this.vertices = [];
        
        this.load();
        this.mesh.material = this.materials[0];
    }

    update(dt, scene, entity){
        super.update(dt, scene, entity);

        this.state.update(dt / 1000);
        this.state.apply(this.skeleton);
        this.skeleton.updateWorldTransform();

        this.updateGeometry();
    }
	
	render(dt, scene, viewport, camera, entity){
		super.render(dt, scene, viewport, camera, entity);

		this.mesh.render(dt, scene, viewport, camera, entity, this);
	}

    load(){
        this.materials = [];

        // Create a new material 
        for (let texture of this.textures) {
            let newMaterial = new Onyx.MaterialPhong("", {
                lighting: this.lighting,
                diffuseTexture: texture
            });

            this.materials.push(newMaterial);
        }
    }

    updateGeometry() {
        let tempPos = this.tempPos;
        let tempUv = this.tempUv;
        let tempLight = this.tempLight;
        let tempDark = this.tempDark;

        var numVertices = 0;
        var verticesLength = 0;
        var indicesLength = 0;

        let blendMode = null;
        let clipper = this.clipper;

        let vertices = this.vertices;
        let triangles = null;
        let uvs = null;
        let drawOrder = this.skeleton.drawOrder;

        let batchVertices = [];
        let batchUVs = [];

        let z = 0;
        let zOffset = this.zOffset;
        for (let i = 0, n = drawOrder.length; i < n; i++) {
            let vertexSize = this.clipper.isClipping() ? 2 : VERTEX_SIZE;
            let slot = drawOrder[i];
            let attachment = slot.getAttachment();
            let attachmentColor = null;
            let texture = null;
            let numFloats = 0;
            if (attachment instanceof spine.RegionAttachment) {
                let region = attachment;
                attachmentColor = region.color;
                vertices = this.vertices;
                numFloats = vertexSize * 4;
                region.computeWorldVertices(slot.bone, vertices, 0, vertexSize);
                triangles = QUAD_TRIANGLES;
                uvs = region.uvs;
                // texture = <ThreeJsTexture>(<TextureAtlasRegion>region.region.renderObject).texture;
                texture = this.mesh.material.diffuseTexture;
            } else if (attachment instanceof spine.MeshAttachment) {
                let mesh = attachment;
                attachmentColor = mesh.color;
                vertices = [];
                numFloats = (mesh.worldVerticesLength >> 1) * vertexSize;
                if (numFloats > vertices.length) {
                    vertices = spine.Utils.newFloatArray(numFloats);
                }
                mesh.computeWorldVertices(slot, 0, mesh.worldVerticesLength, vertices, 0, vertexSize);
                triangles = mesh.triangles;
                uvs = mesh.uvs;
    //             texture = <ThreeJsTexture>(<TextureAtlasRegion>mesh.region.renderObject).texture;
                texture = this.mesh.material.diffuseTexture;
            } else if (attachment instanceof spine.ClippingAttachment) {
                let clip = (attachment); //SkeletonClipping?
                clipper.clipStart(slot, clip);
                continue;
            } else continue;

            if (texture != null) {
                let skeleton = slot.bone.skeleton;
                let skeletonColor = skeleton.color;
                let slotColor = slot.color;
                let alpha = skeletonColor.a * slotColor.a * attachmentColor.a;
                let color = this.tempColor;
                color.set(skeletonColor.r * slotColor.r * attachmentColor.r,
                        skeletonColor.g * slotColor.g * attachmentColor.g,
                        skeletonColor.b * slotColor.b * attachmentColor.b,
                        alpha);

                let finalVertices = [];
                let finalVerticesLength = 0;
                let finalIndices = [];
                let finalIndicesLength = 0;

                if (clipper.isClipping()) {
                    clipper.clipTriangles(vertices, numFloats, triangles, triangles.length, uvs, color, null, false);
                    let clippedVertices = clipper.clippedVertices;
                    let clippedTriangles = clipper.clippedTriangles;
                    if (this.vertexEffect != null) {
                        let vertexEffect = this.vertexEffect;
                        let verts = clippedVertices;
                        for (let v = 0, n = clippedVertices.length; v < n; v += vertexSize) {
                            tempPos.x = verts[v];
                            tempPos.y = verts[v + 1];
                            tempLight.setFromColor(color);
                            tempDark.set(0, 0, 0, 0);
                            tempUv.x = verts[v + 6];
                            tempUv.y = verts[v + 7];
                            vertexEffect.transform(tempPos, tempUv, tempLight, tempDark);
                            verts[v] = tempPos.x;
                            verts[v + 1] = tempPos.y;
                            verts[v + 2] = tempLight.r;
                            verts[v + 3] = tempLight.g;
                            verts[v + 4] = tempLight.b;
                            verts[v + 5] = tempLight.a;
                            verts[v + 6] = tempUv.x;
                            verts[v + 7] = tempUv.y;
                        }
                    }
                    finalVertices = clippedVertices;
                    finalVerticesLength = clippedVertices.length;
                    finalIndices = clippedTriangles;
                    finalIndicesLength = clippedTriangles.length;
                } else {
                    let verts = vertices;
                    if (this.vertexEffect != null) {
                        let vertexEffect = this.vertexEffect;
                        for (let v = 0, u = 0, n = numFloats; v < n; v += vertexSize, u += 2) {
                            tempPos.x = verts[v];
                            tempPos.y = verts[v + 1];
                            tempLight.setFromColor(color);
                            tempDark.set(0, 0, 0, 0);
                            tempUv.x = uvs[u];
                            tempUv.y = uvs[u + 1];
                            vertexEffect.transform(tempPos, tempUv, tempLight, tempDark);
                            verts[v] = tempPos.x;
                            verts[v + 1] = tempPos.y;
                            verts[v + 2] = tempLight.r;
                            verts[v + 3] = tempLight.g;
                            verts[v + 4] = tempLight.b;
                            verts[v + 5] = tempLight.a;
                            verts[v + 6] = tempUv.x;
                            verts[v + 7] = tempUv.y;
                        }
                    } else {
                        for (let v = 2, u = 0, n = numFloats; v < n; v += vertexSize, u += 2) {
                            verts[v] = color.r;
                            verts[v + 1] = color.g;
                            verts[v + 2] = color.b;
                            verts[v + 3] = color.a;
                            verts[v + 4] = uvs[u];
                            verts[v + 5] = uvs[u + 1];
                        }
                    }
                    finalVertices = vertices;
                    finalVerticesLength = numFloats;
                    finalIndices = triangles;
                    finalIndicesLength = triangles.length;
                }

                if (finalVerticesLength == 0 || finalIndicesLength == 0)
                    continue;

                let verts = this.convertQuadsToTriangles(finalVertices, finalIndices, z, vertexSize, this.mesh.material.diffuseTexture);
                batchVertices = batchVertices.concat(verts.vertices);
                batchUVs = batchUVs.concat(verts.uvs);
                z += zOffset;
            }

        }

        if(this.mesh.vertices.length !== batchVertices.length){
            this.mesh.vertices = new Float32Array(batchVertices);
            this.mesh.normals = new Float32Array(batchVertices.length);
            this.mesh.normals.fill(0.5);
        }else{
            this.mesh.vertices.set(batchVertices);
        }

        if(this.mesh.uvs.length !== batchUVs.length){
            this.mesh.uvs = new Float32Array(batchUVs);
        }else{
            this.mesh.uvs.set(batchUVs);
        }

        this.mesh.invalidate(true, true, true);
    }

    convertQuadsToTriangles(vertices, indices, z, vertexSize, texture){
        let verts = [];
        let uvs = [];

        // Convert face to trangle
        for(var i = 0; i < indices.length; i++){
            verts.push(vertices[(indices[i] * vertexSize) + 0], vertices[(indices[i] * vertexSize) + 1], z);
        
            let width = 1;//texture.img.width;
            let height = 1;//texture.img.height;
            uvs.push(vertices[(indices[i] * vertexSize) + (vertexSize - 2)] / width, vertices[(indices[i] * vertexSize) + (vertexSize - 1)] / height);
        }

        return {
            vertices: verts,
            uvs: uvs
        };
    }
}