'use strict';

import * as Onyx from '../lib/onyx/scripts/main.js';
import { spine } from '../lib/spine-ts/build/spine-core';

import { default as SpineModel } from './spine';

export default class ImportSpine extends Onyx.Importer {
    /**
     * After fetching the file, parse the model, animations and textures using the Spine library
     * @returns Spine Model
     */
    async load(){
        let asset = {};
        try{
            asset = JSON.parse(this.input);
        }catch(e){
            throw new Error('[Onyx-Spine] Error while parsing JSON', e);

            return;
        }
        // console.log(asset);

        let atlas = await this.loadTextureAtlas(this.options.atlas);
        // console.log(atlas);
        
        let spineModel = new SpineModel({data: asset, atlas: atlas.atlas, textures: atlas.textures});

        return spineModel;
    }

    /**
     * Fetches the atlas file and creates a list of textures
     * @param {String} atlasSrc 
     * @returns {Object} { atlas, textures }
     */
    async loadTextureAtlas(atlasSrc){
        let atlasData = await this.fetchFromURL(atlasSrc);
        let atlasPages = [];
        let atlasTextures = [];

        // Since this is an async function, and Spine-TS is sync, we have to parse twice
        // TODO: See if we can circumvent
        let atlas = new spine.TextureAtlas(atlasData, (path) => {
            atlasPages.push(path);

            // TODO: Not sure why this is needed, but it's what Spine suggests. Wonder if we can get rid of it.
            let image = document.createElement("img");
            image.width = 16;
            image.height = 16;
            return new spine.FakeTexture(image);
        });

        for (let atlasPage of atlasPages) {
            let texture = new Onyx.Texture(this.resPath + "/" + atlasPages, {flipY: false});
            await texture.load();

            atlasTextures.push(texture);
        }

        atlas = new spine.TextureAtlas(atlasData, (path) => {
            atlasPages.push(path);

            for(let atlasTexture of atlasTextures){
                if(atlasTexture.img.src.split("/").pop() == path){
                    return new spine.FakeTexture(atlasTexture.img);
                }
            }

            // Return default
            let image = document.createElement("img");
            image.width = 16;
            image.height = 16;
            return new spine.FakeTexture(image);
        });

        return {
            atlas: atlas,
            textures: atlasTextures
        };
    }
}