'use strict';

import { mat4, vec3, vec4, quat } from '../lib/gl-matrix-master/src/gl-matrix';

export default class AnimationTrack {
	constructor({name = "", values = [[]], times = [], type = "position", parent = null} = {}) {
        this.name = name;
        this.values = values;
        this.times = times;
        this.type = type;

        this.parent = parent;
        this.currentTime = 0.0;
        this.currentFrameValues = null;
        this.currentFrameTime = null;
        this.currentFrameIndex = -1;
        this.nextFrameValues = this.values[0];
        this.nextFrameTime = this.times[0];
        this.nextFrameIndex = 0;

        this.complete = false;
    }
    
    update(dt, currentClipTime){
        if(this.complete) return;

        //TODO: Support rewind
        this.currentTime += dt;// * 100;

        if(currentClipTime > this.nextFrameTime){
            // Find next frame
            this.currentFrameValues = this.nextFrameValues;
            this.currentFrameTime = this.nextFrameTime;
            this.currentFrameIndex = this.nextFrameIndex;

            this.nextFrameIndex++;
            this.nextFrameValues = this.values[this.nextFrameIndex];
            this.nextFrameTime = this.times[this.nextFrameIndex];

            if(this.nextFrameIndex >= this.times.length){
                this.complete = true;
                return;
            }

            // console.log(this.currentFrameIndex, this.nextFrameIndex);
        }

        // TODO Fix Percent for last frame
        if(this.currentFrameValues) this.interpolateLinear(this.currentFrameValues, this.nextFrameValues, (currentClipTime - this.currentFrameTime) / (this.nextFrameTime - this.currentFrameTime));
    }

    interpolateLinear(currentFrame, nextFrame, percent){
        switch(this.type){
            case "position":
                this.parent.transform.translateTo(
                    currentFrame[0] + ((nextFrame[0] - currentFrame[0]) * percent),
                    currentFrame[1] + ((nextFrame[1] - currentFrame[1]) * percent),
                    currentFrame[2] + ((nextFrame[2] - currentFrame[2]) * percent) 
                );
                break;
            case "scale":
                this.parent.transform.scaleTo(
                    currentFrame[0] + ((nextFrame[0] - currentFrame[0]) * percent),
                    currentFrame[1] + ((nextFrame[1] - currentFrame[1]) * percent),
                    currentFrame[2] + ((nextFrame[2] - currentFrame[2]) * percent) 
                );
                break;
            case "rotation":
                //TODO: Support other rotation orders
                switch(nextFrame[3]){
                    case "ZYX":
                        this.parent.transform.rotateToZYX(
                            currentFrame[2] + ((nextFrame[2] - currentFrame[2]) * percent),
                            currentFrame[1] + ((nextFrame[1] - currentFrame[1]) * percent),
                            currentFrame[0] + ((nextFrame[0] - currentFrame[0]) * percent)
                        );
                        break;
                    case "XYZ":
                    default:
                        this.parent.transform.rotateToXYZ(
                            currentFrame[0] + ((nextFrame[0] - currentFrame[0]) * percent),
                            currentFrame[1] + ((nextFrame[1] - currentFrame[1]) * percent),
                            currentFrame[2] + ((nextFrame[2] - currentFrame[2]) * percent) 
                        );
                        break;
                }
                break;
            case "quaternion":
                quat.slerp(this.parent.transform.rotationQuat, currentFrame, nextFrame, percent);
                this.parent.transform.refreshMat4();

                // this.parent.transform.rotationQuat = [
                //     currentFrame[0] + ((nextFrame[0] - currentFrame[0]) * percent),
                //     currentFrame[1] + ((nextFrame[1] - currentFrame[1]) * percent),
                //     currentFrame[2] + ((nextFrame[2] - currentFrame[2]) * percent),
                //     currentFrame[3] + ((nextFrame[3] - currentFrame[3]) * percent)];
                // this.parent.transform.refreshMat4();
                // this.parent.transform.generateTranslationRotationScale();

                // this.parent.transform.rotateToXYZ(
                //     currentFrame[0] + ((nextFrame[0] - currentFrame[0]) * percent),
                //     currentFrame[1] + ((nextFrame[1] - currentFrame[1]) * percent),
                //     currentFrame[2] + ((nextFrame[2] - currentFrame[2]) * percent) 
                // );
                break;
        }
    }

    reset(){
        this.nextFrameIndex = 0;
        this.currentTime = 0;
        this.nextFrameValues = this.values[this.nextFrameIndex];
        this.nextFrameTime = this.times[this.nextFrameIndex];

        this.complete = false;
    }
}