'use strict';

export default class AnimationClip {
	constructor({ name = "", tracks = [], enabled = false } = {}) {
        this.name = name;
        this.tracks = tracks;

        this.enabled = enabled; // TODO: Should be false by default

        this.rate = 1;
        this.currentTime = 0;
    }
    
    update(dt){
        if(!this.enabled) return;

        this.currentTime += dt * this.rate;

        var allComplete = true;
        for(var i = 0; i < this.tracks.length; i++){
            this.tracks[i].update(dt, this.currentTime);

            if(!this.tracks[i].complete) allComplete = false;
        }
        
        if(allComplete){
            for(var i = 0; i < this.tracks.length; i++){
                this.tracks[i].reset();
            }
            this.currentTime = 0;
        }
    }
}