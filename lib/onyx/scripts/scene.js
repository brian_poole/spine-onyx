'use strict';

import Color from './color';
import Entity from './entity';
import Timer from './timer';
import Viewport from './viewport';

export default class Scene extends Entity {
    constructor({template = null, scene = {}, templateURL = null, sceneURL = null, elementId = "", clock = null} = {}){
        super();
        
		this.entities = [];
		this.removeEntities = []; // For deferred removal

        this.viewports = [];
        this.viewportElements = [];

        this.templateURL = templateURL; // TODO: Should this whole "template" thing be in a "Route" class?
        this.sceneURL = sceneURL;

        this.template = template;
        this.scene = scene;

        this.clock = clock;

        this.ambientLightColor = new Color(0.3, 0.3, 0.3, 1.0);
        this.directionalLightColor = new Color(1.0, 1.0, 1.0, 1.0);
        this.directionalLightVector = new Float32Array([0.85, 0.8, 0.75]);

        this.fogColor = new Color(1.0, 1.0, 1.0, 1.0);
        this.fogDensity = 0.0;

        if(elementId){
            this.element = document.getElementById(elementId);
            if(!this.element) console.warn (`[Onyx] Element '${elementId}' not found`);
        }

        if(clock){
            clock.add(this);
        }

        this.loadSceneURL();
        this.loadTemplateURL();
    }

    registerHandler(event){
        // Get element
        var element = (event.elementId)?document.getElementById(event.elementId):document.body;
        if(!element){
            console.warn(`[Onyx] Element "${event.elementId}" not found`);
            return;
        }

        // Assign event to element
        //TODO Error checking / Remove need for setTimeout
        window.setTimeout(()=>{
            event.callback = event.target[event.property].bind(this);
            if(element.addEventListener){
                element.addEventListener(event.event, event.callback);
            }else{
                element[event.event] = event.callback ;
            }
        }, 0);

    }

    unregisterHandler(event){
        // Get element
        var element = (event.elementId)?document.getElementById(event.elementId):document.body;
        if(!element){
            console.warn(`[Onyx] Element "${event.elementId}" not found`);
            return;
        }

        // Unassign event to element
        if(element.addEventListener){
            element.removeEventListener(event.event, event.callback);
        }else{
            element[event.event] = null;
        }
    }

    modalResult(result){
        if(this._modal){
            this._modal.callee.modalResult(result, this._modal);
        } 
    }

    loadSceneURL(url = this.sceneURL){
        if(!url) return false;

        this.fetchTemplate(url).then((response) => {
            this.scene = response;
        });
    }

    loadTemplateURL(url = this.templateURL){
        if(!url) return false;

        this.fetchTemplate(url).then((response) => {
            this.template = response;
        });
    }

    onload(...args){
        super.onload(...args);

        if(this.element) this.queryDOM(this.element);
    }

    onunload(...args){
        super.onunload(...args);

        // TODO Call onDestroy for everything

        this.entities = [];
        this.viewports = [];
        this.viewportElements = [];
        this.components = [];

        if(this.element){
            this.element.innerHTML = "";
            this.element.style.display = "none";
        }
    }

    populateDOM(elem){
        if(this.template !== null && this.template !== undefined) elem.innerHTML = this.template;

        if(this.element) this.element.style.display = "block";

        this.queryDOM(this.element);
    }

    clearDom(elem){
        if(elem) elem.innerHTML = "";
    }

    queryDOM(elem = document.body){
        // Grab all viewports
        this.viewportElements = [];

        
        // Note: CocoonJS doesn't fully support querySelectorAll, so we're doing this manually, rather than querySelectorAll
        // Any <onyx-viewport> tags
        let nodes = elem.getElementsByTagName("onyx-viewport");
        for(var i = 0; i < nodes.length; i++){
            // TODO(?): use this element to create a canvas inside of it
            this.viewportElements.push(nodes[i]);
        }

        // Any <ANY onyx-viewport> tags

        if(typeof document.querySelectorAll !== "undefined" && !navigator.isCocoonJS){
            nodes = elem.querySelectorAll("[onyx-viewport]");

            for(var i = 0; i < nodes.length; i++){
                this.viewportElements.push(nodes[i]);
            }
        }else{
            // Fallback for older browsers / CocoonJS
            nodes = elem.getElementsByTagName("*");

            for(var i = 0; i < nodes.length; i++){
                var node = nodes[i];
                if(typeof node["onyx-viewport"] !== "undefined" || (node.hasAttribute && node.hasAttribute("onyx-viewport"))) {
                    this.viewportElements.push(node);
                }
            }
        }
        
        // Assign a new viewport for each one
        for(let elem of this.viewportElements){
            this.viewports.push(new Viewport(elem));    
        }
    }

    render(dt){
        for(let viewport of this.viewports){
            viewport.render(dt, this);
        }

        // TODO: Update DOM elements
    }
	
	/**
	 * Called every logic cycle
	 */
	update(deltaTime) {
        super.update(this, deltaTime);
        
        if(!this.enabled) return;
        
		for(let entity of this.entities){
			entity.update(deltaTime, this);	
		}

		for(let entity of this.removeEntities){
			this.remove(entity, false);	
		}
		this.removeEntities = [];
	}

    add(entity){
        this.entities.push(entity);
		if(entity.onSpawn) entity.onSpawn(this);
    }

    remove(entity, defer){ //TODO: make defer an option object?
		if(defer){
			this.removeEntities.push(entity);
		}else{
			if(this.entities.indexOf(entity) > -1){
				this.entities.splice(this.entities.indexOf(entity), 1);
				if(entity.onDestroy) entity.onDestroy(this);
			}
		}
    }

    start(){
        if(!this.clock) return;

        this.clock.start();
        // this.onload();
    }

    stop(){
        if(!this.clock) return;

        this.clock.stop();
        // this.onunload();
    }

	setTimeout( callback, delay ) {
		let timer = new Timer();
		this.add( timer );
        
        timer.setTimeout(( entity, dt, progress ) => {
            this.remove(timer);
			if(callback) callback();
		}, delay );

		return timer;
	}

	setInterval( callback, delay = 0, intervalCount = 0 ) {
		let timer = new Timer();
		this.add( timer );
        
        timer.setInterval(( entity, dt, progress ) => {
			if(callback) callback();
		}, delay, intervalCount );

		return timer;
	}

	setModulate( callback, delay ) {
		let timer = new Timer();
		this.add( timer );
        
        timer.setModulate(( entity, dt, progress ) => {
			if(callback) callback(entity, dt, progress);
		}, delay );

		return timer;
	}

    sortEntitiesByZ(inverse){
        if(inverse){
            this.entities.sort(function(a, b) {
                return (a.transform && b.transform) && a.transform.z - b.transform.z;
            });
        }else{
            this.entities.sort(function(a, b) {
                return (!a.transform || !b.transform) || b.transform.z - a.transform.z;
            });
        }
    }
    
    // Fetch Template
    fetchTemplate(url){
        return fetch(url, {
            "Content-Type": "text/html"
        }).then((response) => {
            // Read response
            if (!response.ok) {
                throw Error(response.statusText, response.url);
            }
            return response.text();
        }).then(
            // Return contents
            (contents) => contents
        ).catch((e) => {
            console.error(`Error fetching template '${url}'.`, e);
            // this.loadView('__loadingFailure__');
        });
    }
}