'use strict';

import { mat4 } from '../lib/gl-matrix-master/src/gl-matrix';
import GLProgram from './gl-program';
import Color from './color';

let id = 0;

export default class Viewport {
	get nearPlane() { return this._nearPlane; }
	set nearPlane(value) {
		this._nearPlane = value;
		this.evaluate();
		return this._nearPlane;
	}
	
	get farPlane() { return this._farPlane; }
	set farPlane(value) {
		this._farPlane = value;
		this.evaluate();
		return this._farPlane;
	}

	get projectionType() { return this._projectionType; }
	set projectionType(value) {
		this._projectionType = value;
		this.evaluate();
		return this._projectionType;
	}

	get clearColor() { return this._clearColor; }
	set clearColor(value) {
		this._clearColor = value;
		this.webgl.clearColor(this._clearColor.color[0], this._clearColor.color[1], this._clearColor.color[2], this._clearColor.color[3]);

		return this._clearColor;
	}
	
	constructor( contextElement ) {
		this._currentTexture = [];

		this.id = id++;
		
		this.mouseEventViewport = null;
		this.mouseEventRadius = 1.0;
		
		this.camera = null;
		this.contextElement = contextElement;
		this._clearColor = new Color(0, 0, 0, 0);
		this.clearOnRender = true;
		this.programs = [];
		this.program = null;

		this.frameBufferTextures = [];
		
		// TODO: This should live at the camera
		this.projectionMatrix = mat4.create();
		// TODO: move getAttributes to params + update scene.js
		this._projectionType = contextElement.getAttribute("projection-type") || "Perspective";
		this._nearPlane = (contextElement.getAttribute("nearplane") * 1) || 0;
		this._farPlane = (contextElement.getAttribute("farplane") * 1) || 10000.0;

		this.orthoWidth = (contextElement.getAttribute("ortho-width") * 1) || 0;
		this.orthoHeight = (contextElement.getAttribute("ortho-height") * 1) || 0;

		this.parallax = (contextElement.getAttribute("parallax") == "true");
		
		// Initialize WebGL
		if(!window.WebGLRenderingContext) throw new Error("[Onyx] WebGL is not supported by this browser.");
		
		try {
			this.webgl = contextElement.getContext("webgl", {
				premultipliedAlpha: false  // Ask for non-premultiplied alpha
			  }) || contextElement.getContext("experimental-webgl", {
				premultipliedAlpha: false  // Ask for non-premultiplied alpha
			  });
		} catch(e) {
			throw new Error("[Onyx] Couldn't initialize WebGL");
			return;
		}

		if(!this.webgl){
			throw new Error("[Onyx] WebGL appears to be disabled");
			return;
		}
		
		// Get the viewport ready...
		this.webgl.viewport(0, 0, contextElement.width, contextElement.height);
		this.webgl.clearColor(this._clearColor.color[0], this._clearColor.color[1], this._clearColor.color[2], this._clearColor.color[3]);
		this.webgl.blendFuncSeparate(this.webgl.SRC_ALPHA, this.webgl.ONE_MINUS_SRC_ALPHA, this.webgl.ZERO, this.webgl.ONE);

		mat4.identity(this.projectionMatrix);
		
		this.clear();
		
		// Watch for changes
		window.addEventListener("resize", () => {
			this.evaluate();
		});

		this.evaluate();
	}
	
	clear() {
		this.webgl.clear(this.webgl.COLOR_BUFFER_BIT | this.webgl.DEPTH_BUFFER_BIT);
	}
		
	evaluate(clear = true) {
		if(clear){
			if(!navigator.isCocoonJS){
				if(this.contextElement.clientWidth && this.contextElement.clientHeight){
					this.contextElement.width = this.contextElement.clientWidth;
					this.contextElement.height = this.contextElement.clientHeight;
				}
			}else{
				this.contextElement.width = window.innerWidth * window.devicePixelRatio;
				this.contextElement.height = window.innerHeight * window.devicePixelRatio;
			}
			this.webgl.viewport(0, 0, this.contextElement.width, this.contextElement.height);
		}

		//TODO this should be at the camera level, along with FOV, etc.
		if(this._projectionType.toLowerCase() == "orthographic"){
			// Solve width / height
			let width = this.orthoWidth;
			let height = this.orthoHeight;
			
			if(!width && !height){
				width = this.contextElement.width;
				height = this.contextElement.height;
			}else{
				let aspect = this.contextElement.width / this.contextElement.height;

				if(!width){
					width = height * aspect;
				}else if(!height){
					height = width / aspect;
				}
			}

			this.width = width;
			this.height = height;

			mat4.ortho(this.projectionMatrix, 0, width, 0, height, this._nearPlane, this._farPlane);
		}else{
			mat4.perspective(this.projectionMatrix, 70, this.contextElement.width / this.contextElement.height, this._nearPlane, this._farPlane);
		}
		
		// Set the projection matrix
		if(this.program){
			this.webgl.uniformMatrix4fv(this.program.uniform.uPMatrix, false, this.projectionMatrix);
		}
	}
	
	render(dt, scene) {
		if(this.clearOnRender) this.clear();

		for(let i = 0; i < this.frameBufferTextures.length; i++){
			this.frameBufferTextures[i].render(dt, scene, this);
		}
		
		if(this.camera) this.camera.render(dt, scene, this);
	}

	renderPick(mouseX, mouseY, event, radius = 1.0) {
		if(!this.camera) return;
	
	    this.mouseEventViewport.contextElement.width = this.contextElement.width || this.contextElement.naturalWidth;
	    this.mouseEventViewport.contextElement.height = this.contextElement.height || this.contextElement.naturalHeight;
	    this.mouseEventViewport.evaluate(true);
	    this.mouseEventViewport.canvas2d.width = this.contextElement.width || this.contextElement.naturalWidth;
	    this.mouseEventViewport.canvas2d.height = this.contextElement.height || this.contextElement.naturalHeight;
	    this.mouseEventViewport.webgl.clear(this.webgl.COLOR_BUFFER_BIT | this.webgl.DEPTH_BUFFER_BIT);
	    
		this.mouseEventViewport.clear();
        // this.mouseEventViewport.webgl.disable(this.webgl.GL_BLEND);
	    
		this.camera.renderScenePick( this.mouseEventViewport );

		let entityList = [];

		mouseX -= radius / 2;
		mouseY -= radius / 2;

        // var rgbaData = new Uint8Array(radius * radius * 4);
        // this.webgl.readPixels(mouseX | 0, mouseY | 0, radius, radius, this.webgl.RGBA, this.webgl.UNSIGNED_BYTE, rgbaData);
        
        // Copy scene to 2D Canvas (TODO: Is this faster if we read from the webgl buffer?)
		this.mouseEventViewport.context2d.drawImage(this.mouseEventViewport.contextElement, 0, 0);
        var rgbaData = this.mouseEventViewport.context2d.getImageData(mouseX | 0, mouseY | 0, radius, radius).data;
        
		for(var i = 0; i < rgbaData.length; i++){
			// Find entity by RGB data
			var entity = this.camera.stage.entities[rgbaData[i] - 1];
			
			// Add entity to the candidates list
			if(entity) entityList.push(entity);
		}
		

		// Find most likely candidate
		let scores = [];
		for(let entity of entityList){
			if(!scores[this.camera.stage.entities.indexOf(entity)]) scores[this.camera.stage.entities.indexOf(entity)] = 0;

			scores[this.camera.stage.entities.indexOf(entity)]++;
		}
		
		let largest = 0;
		let largestEntity = 0;
		for(let i in scores){
			if(scores[i] > largest){
				largest = scores[i];
				largestEntity = i;
			}
		}

		if(this.camera.stage.entities[largestEntity]) this.camera.stage.entities[largestEntity].emit('click', event);
	}

	createProgram( material ) {
		for(let program of this.programs){
			if(program.vertexShader == material.vertexShader && program.fragmentShader == material.fragmentShader){
				return program;
			} 
		}

		// Create a new program (if one doesn't already exist)
		this.programs.push(new GLProgram(this));

		let program = this.programs[this.programs.length - 1];
		
		// Compile shaders
		program.addVertexShader(this, material.vertexShader);
		program.addFragmentShader(this, material.fragmentShader);
		
		// Links the compiled shaders to the program
		this.webgl.linkProgram(program.program);	
		
		// Get attributes from shaders
		program.getAttributesFromShaders(this, material.shaderAttributes);
		program.getUniformLocations(this, material.shaderUniforms);

		return program;
	}
	
	useProgram( program ) {
		// if(this.program === program) return;
		
		this.program = program;
		
		this.program.use(this);
		this.webgl.uniformMatrix4fv(this.program.uniform.uPMatrix, false, this.projectionMatrix);
	}
	
	bindToCamera( camera ) {
		this.camera = camera;
		camera.addViewport(this);
		this.evaluate();
	}
	
	listenForMouseEvents( mouseRadius = 1.0 ){
	    let videoBufferCanvas = document.createElement("canvas");
	    
		this.mouseEventViewport = new Viewport(videoBufferCanvas);
		this.mouseEventRadius = mouseRadius;
		
		// Initialize 2D
		try {
			this.mouseEventViewport.canvas2d = document.createElement('canvas');
			this.mouseEventViewport.context2d = this.mouseEventViewport.canvas2d.getContext("2d");
		} catch(e) {
			console.error("Couldn't initialize 2D");
		}
	    
		// Watch for mouse events
		this.contextElement.addEventListener("mousedown", (event) => {
			this.renderPick(event.offsetX, event.offsetY, event, this.mouseEventRadius);
		});
		
		this.contextElement.addEventListener("touchstart", (event) => {
// 			console.log(event);
			this.renderPick(Math.round(event.targetTouches[0].pageX), Math.round(event.targetTouches[0].pageY), event, event.targetTouches[0].radiusX);
		});
	}

	addFrameBufferTexture(frameBufferTexture){
		this.frameBufferTextures.push(frameBufferTexture);
	}

	removeFrameBufferTexture(frameBufferTexture){
		if(this.frameBufferTextures.indexOf(frameBufferTexture) > -1) 
			this.frameBufferTextures.splice(this.frameBufferTextures.indexOf(frameBufferTexture), 1);
	}
}