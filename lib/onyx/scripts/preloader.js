'use strict';

// import ModelCache from './model-cache';
// import TextureCache from './texture-cache';
// import AudioCache from './model-cache';

import Texture from './texture';

export default class Preloader {
	constructor(){
        // TODO, keep track of pending textures and/or count for progress reports.
	}
    
    preloadTextures(srcList = [], progressCallback) {
		let pending = srcList.length;
        
        return new Promise((resolve, reject) => {
            if(!pending) resolve();
            
            for(let src of srcList){
                new Texture(src).load().then(() => {
                    pending--;

                    if(progressCallback){
                        let percent = (srcList.length - pending) / srcList.length;
                        progressCallback(percent, srcList.length - pending);
                    }
                    
                    if(!pending) resolve();
                }).catch((...args) => {
                    reject(...args);
                });
            }
        });
    }
    
    // preloadModels(srcList = [], progressCallback) {
	// 	let pending = srcList.length;
        
    //     return new Promise((resolve, reject) => {
    //         if(!pending) resolve();
            
    //         for(let src of srcList){
    //             ModelCache.load(src).then(() => {
    //                 pending--;

    //                 if(progressCallback){
    //                     let percent = (srcList.length - pending) / srcList.length;
    //                     progressCallback(percent, srcList.length - pending);
    //                 }
                    
    //                 if(!pending) resolve();
    //             }).catch((...args) => {
    //                 reject(...args);
    //             });
    //         }
    //     });
    // }
    
//      preloadAudio(srcList = []) {
// 		let pending = 0;
        
//         return new Promise(function(resolve, reject) {
//             if(srcList.length <= 0) resolve();
            
//             for(let modelSrc of srcList){
//                 AudioCache.load(modelSrc).then(() => {
//                     pending--;
                    
//                     if(!pending) resolve();
//                 });
//             }
//         });
//     }
}