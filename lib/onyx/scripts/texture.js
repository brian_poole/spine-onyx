'use strict';

export default class Texture {
	constructor(src = "", {flipY = true, magFilter = "LINEAR", minFilter = "LINEAR_MIPMAP_LINEAR"} = {}) {
		this.src = src;

		this._loadPromise = null;
		this.loaded = false;

		this.texture = [];
		this.img = null;

		this.flipY = flipY;
		this.magFilter = magFilter;
		this.minFilter = minFilter;

		this.load();
	}

	getImage(){
		return this.img;
	}

	load() {
		if(this._loadPromise) return this._loadPromise;
		
		this.img = new Image();
		this._loadPromise = new Promise((resolve, reject) => {
			this.img.onload = (() => {
				this.loaded = true;
				this._loadPromise = null;
				resolve(this);
			});
			this.img.onerror = ((e) => {
				this.loaded = false;
				this._loadPromise = null;
				reject(`Error loading image file: '${this.img.src}'`);
			});
		});

		this.img.src = this.src;
		return this._loadPromise;
	}

	unload ( viewport ) {
		viewport.webgl.deleteTexture(this.texture);
	}
	
	updateTexture( viewport ) {
		viewport.webgl.bindTexture(viewport.webgl.TEXTURE_2D, this.texture[viewport.id]);
		viewport.webgl.pixelStorei(viewport.webgl.UNPACK_FLIP_Y_WEBGL, this.flipY);
		viewport.webgl.texImage2D(viewport.webgl.TEXTURE_2D, 0, viewport.webgl.RGBA, viewport.webgl.RGBA, viewport.webgl.UNSIGNED_BYTE, this.img);
		viewport.webgl.generateMipmap(viewport.webgl.TEXTURE_2D);

		// For video
		// if(this.video.readyState === 4){
		// 	viewport.webgl.texImage2D(viewport.webgl.TEXTURE_2D, 0, viewport.webgl.RGBA, viewport.webgl.RGBA, viewport.webgl.UNSIGNED_BYTE, this.video);
		// }
		//viewport.webgl.texParameteri(viewport.webgl.TEXTURE_2D, viewport.webgl.TEXTURE_MAG_FILTER, viewport.webgl.NEAREST);
		//viewport.webgl.texParameteri(viewport.webgl.TEXTURE_2D, viewport.webgl.TEXTURE_MIN_FILTER, viewport.webgl.NEAREST);
	}

	setAsTexture( viewport, textureNumber = 0, uniformPosition = null){
		if(!this.img){ return console.error('Texture has no image loaded'); }
		if(!this.loaded){ return console.warn('Texture image not yet loaded'); }
		if(viewport._currentTexture[textureNumber] == this) return;
		
		// Create the texture if it doesn't exist yet
		if(!this.texture[viewport.id]){
			this.texture[viewport.id] = viewport.webgl.createTexture();
			this.updateTexture(viewport);
			
			viewport.webgl.bindTexture(viewport.webgl.TEXTURE_2D, null);
		}

		viewport.webgl.activeTexture(viewport.webgl["TEXTURE"+textureNumber]);
		viewport.webgl.bindTexture(viewport.webgl.TEXTURE_2D, this.texture[viewport.id]);

		// Allow non-square textures
		viewport.webgl.texParameteri(viewport.webgl.TEXTURE_2D, viewport.webgl.TEXTURE_WRAP_S, viewport.webgl.CLAMP_TO_EDGE);
		viewport.webgl.texParameteri(viewport.webgl.TEXTURE_2D, viewport.webgl.TEXTURE_WRAP_T, viewport.webgl.CLAMP_TO_EDGE);

		viewport.webgl.texParameteri(viewport.webgl.TEXTURE_2D, viewport.webgl.TEXTURE_MAG_FILTER, viewport.webgl[this.magFilter]);
		viewport.webgl.texParameteri(viewport.webgl.TEXTURE_2D, viewport.webgl.TEXTURE_MIN_FILTER, viewport.webgl[this.minFilter]);
		
		if(uniformPosition){
			viewport.webgl.uniform1i(uniformPosition, textureNumber);
		}

		viewport._currentTexture[textureNumber] = this;

		return true;
	};
}