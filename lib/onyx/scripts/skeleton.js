'use strict';

import { mat4 } from '../lib/gl-matrix-master/src/gl-matrix';

import Transform from './transform';

const BONE_MAX = 4;

export default class Skeleton {
	constructor({transform = new Transform(), children = [], bones = []} = {}) {
        this.transform = transform;
        
        this.bones = bones;
        this.children = children;
        
        this.matrixBuffer = new Float32Array(BONE_MAX * 16);
        this.scaledMatrix = mat4.create();
    }

    update(dt, scene, entity){
        for(var i = 0; i < this.children.length; i++){
            this.children[i].update(dt, scene, entity);
        }
    }

    calcBindPose(){
        for(var i = 0; i < this.bones.length; i++){
            this.bones[i].calcOrigin(true);
        }
    }

    getFlatMatrix(worldMatrix){        
        let offset = 0;

        // mat4.scale(this.scaledMatrix, );

        for(var i = 0; i < this.bones.length; i++){
            offset = this.bones[i].calcMatrix(this.transform.mat4, this.matrixBuffer, offset, false);
        }

        return this.matrixBuffer;
    }
}