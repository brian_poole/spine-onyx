'use strict';

export { default as AnimationClip }    	from './animation-clip';
export { default as AnimationTrack }  	from './animation-track';
export { default as Bone }          	from './bone.js';
export { default as Camera }          	from './camera';
export { default as Clock }             from './clock';
export { default as Color }             from './color';
export { default as Component }         from './component';
export { default as Entity }            from './entity';
export { default as FrameBufferTexture2D}from './framebuffer-texture2d.js';
export { default as GLProgram }         from './gl-program';
export { default as Material }          from './material';
export { default as Mesh }              from './mesh';
export { default as Model }             from './model';
export { default as Preloader }         from './preloader';
export { default as Router }            from './router';
export { default as Scene }             from './scene';
export { default as Skeleton }          from './skeleton';
export { default as SkeletalAnimation } from './skeletal-animation';
export { default as Sprite }            from './sprite';
export { default as SpriteAnimation }   from './sprite-animation';
export { default as Transform }         from './transform';
export { default as Texture }           from './texture';
// export { default as Tilemap }           from './tilemap';
export { default as Timer }             from './timer';
export { default as Tween }             from './tween';
export { default as Vertex2 }           from './vertex2';
export { default as Vertex3 }           from './vertex3';
export { default as Video }             from './video';
export { default as Viewport }          from './viewport';

export { default as MaterialPhong }     from './materials/phong';

export { default as Importer }          from './importer';
export { default as ImportModelFromFBX }          from './import/model-fbx';
export { default as ImportModelFromOBJ }          from './import/model-obj';
export { default as ImportModelFromThreeJS }      from './import/model-threejs';
export { default as ImportSpriteFromTexturePacker } from './import/sprite-texturepacker';

export function handles(event, {elementId = elementId} = {elementId : null}) {
    return (target, property, descriptor) => {
        // console.log(target, property, descriptor);

        if(!target._priv){
            target._priv = {events: []};
        }
        target._priv.events.push({target: target, property: property, descriptor: descriptor, event: event, elementId: elementId});
    }
}

export function route(){
    return (target, property, descriptor) => {

    }
}