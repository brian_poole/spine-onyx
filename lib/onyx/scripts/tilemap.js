'use strict';

import Component from './component';
import Mesh from './mesh';
import textureCache from './texture-cache';

import { vec2 } from '../lib/gl-matrix-master/src/gl-matrix';

let face = new Float32Array([
	//x  y  u  v
	0, 0, 0, 1,
	1, 0, 1, 1,
	1, 1, 1, 0,

	0, 0, 0, 1,
	1, 1, 1, 0,
	0, 1, 0, 0
]);

// Might not even need texture face??
let textureFace = new Float32Array([
                0, 0, //  left,    top, 
				1, 0, // right,    top, 
				1, 1, // right,  bottom, 
				0, 1, //  left,  bottom, 
				0, 0, //  left,    top, 
				1, 1  // right,  bottom, 
            ]);

export default class Tilemap extends Component {
	constructor(src, atlas, tileSize = 16.0, atlasPadding = 0.0) {
		super();
			return; // temp
		
		this.texture = null;
		this.atlas = null;
		this.atlasPadding = atlasPadding;
		this.scaledViewportSize = vec2.create();
		this.inverseTextureSize = vec2.create();
		this.inverseSpriteTextureSize = vec2.create();
		
		this.tileScale = 1.0;
        this.tileSize = tileSize;

		this.antiAliasing = false;
		
		this.unitScale = 1;
		
		this.face = face;
		
		this.quadVertBuffer = null;
		
		this.mesh = new Mesh();
		this.mesh.addMaterial("tilemap", face, { uvs: textureFace, texture: atlas });
		this.mesh.materials[0].textureLoad.then(() => {
			this.texture.flipY = false;

			if(!this.antiAliasing){
				this.texture.magFilter = "NEAREST";
				this.texture.minFilter = "NEAREST";
			}
			
			let width = this.texture.image.width * this.tileSize * this.unitScale;
			let height = this.texture.image.height * this.tileSize * this.unitScale;
			
			this.face[4] = width;
			this.face[8] = width;
			this.face[16] = width;
			
			this.face[9] = height;
			this.face[17] = height;
			this.face[21] = height;
		});
	}
	
	render( entity, camera, viewport ) {
			return; // temp
		this.mesh.render(entity, camera, viewport);
	}
}