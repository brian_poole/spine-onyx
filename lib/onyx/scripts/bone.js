'use strict';

import { mat4 } from '../lib/gl-matrix-master/src/gl-matrix';

import Transform from './transform';

const tMatrix = mat4.create();

export default class Bone {
	constructor({name = "", parent = null, children = [], isRoot = false, inheritTransform = true, transform = new Transform()} = {}) {
        this.name = name;
        this.children = children;
        this.isRoot = isRoot;
        this.parent = parent;
        this.inheritTransform = inheritTransform;

        this.currentAnimation = null;
        this.currentAnimationTime = 0.0;
        this.currentFrame = null;
        this.currentFrameIndex = -1;
        this.nextFrame = null;
        this.nextFrameIndex = -1;

        this.transform = transform; // Local matrix
        // this.worldMatrix = mat4.create();
        // this.localMatrix = mat4.create();
        this.inverseBindpose = mat4.create();
        this.offsetMatrix = mat4.create();

        this.calcOrigin();
    }

    update(dt, scene, entity){
        //TODO: Support rewind
        this.currentAnimationTime += dt;// * 100;

        if(this.currentAnimation){
            if(this.currentAnimationTime > this.nextFrame.time){
                // Find next frame
                this.currentFrame = this.nextFrame;
                this.currentFrameIndex = this.nextFrameIndex;

                this.nextFrameIndex++;
                if(this.nextFrameIndex >= this.currentAnimation.keyframes.length){
                    this.nextFrameIndex = 0;
                    this.currentAnimationTime = 0;
                }
                this.nextFrame = this.currentAnimation.keyframes[this.nextFrameIndex];

                // this.currentAnimationTime = 0;

                // console.log(this.currentFrameIndex, this.nextFrameIndex);
            }

            // TODO Fix Percent for last frame
            this.interpolateTransforms(this.currentFrame, this.nextFrame, (this.currentAnimationTime - this.currentFrame.time) / (this.nextFrame.time - this.currentFrame.time));

            // this.transform.rotateToX(tTransform.rot[0]);
            // this.transform.rotateToY(tTransform.rot[1]);
            // this.transform.rotateToZ(tTransform.rot[2]);
        }

        // Update children
        for(var i = 0; i < this.children.length; i++){
            this.children[i].update(dt, scene, entity);
        }
    }

    add(child){
        this.children.add(child);
        child.parent = this;
    }

    interpolateTransforms(currentFrame, nextFrame, percent){
        function linear(start, stop, p){
            return [
                start[0] + ((stop[0] - start[0]) * p),
                start[1] + ((stop[1] - start[1]) * p),
                start[2] + ((stop[2] - start[2]) * p) 
            ]
        }
        let position = linear(currentFrame.position, nextFrame.position, percent);
        let rotation = linear(currentFrame.rotation, nextFrame.rotation, percent);
        let scale = linear(currentFrame.scale, nextFrame.scale, percent);
        
        this.transform.translateTo(position[0], position[1], position[2]);
        this.transform.rotateToXYZ(rotation[0], rotation[1], rotation[2]);
        this.transform.scaleTo(scale[0], scale[1], scale[2]);
    }

    calcOrigin(recursive = false){
        // "Capture" current position relative to vertices
        // mat4.invert(this.inverseBindpose, this.transform.mat4);
        this.calcParentMatrix(tMatrix);
        mat4.invert(this.inverseBindpose, tMatrix);

        // Recursive
        if(recursive){
            for(var i = 0; i < this.children.length; i++){
                this.children[i].calcOrigin(true);
            }
        }
    }

    calcParentMatrix(cMatrix = tMatrix){ //TODO: Confusing named function 
        if(!this.parent || !this.inheritTransform){ // If it's root, use local matrix
            mat4.copy(cMatrix, this.transform.mat4);
        }else{
            this.parent.calcParentMatrix(cMatrix);
            mat4.multiply(cMatrix, cMatrix, this.transform.mat4);
            // mat4.multiply(cMatrix, this.parent.transform.mat4, this.transform.mat4);
        }
    }
    
    calcMatrix(parentMatrix, bufferMatrix, offset, recursive = true){  
        // mat4.copy(tMatrix, this.transform.mat4);
        this.calcParentMatrix(tMatrix);
        mat4.multiply(this.offsetMatrix, tMatrix, this.inverseBindpose); 
        
        // //bone.offsetMatrix = bone.inverseBindpose * bone.worldMatrix
        // mat4.multiply(this.offsetMatrix, this.transform.mat4, this.inverseBindpose); 

        // mat4.multiply(this.offsetMatrix, this.transform.mat4, parentMatrix); 
        // mat4.multiply(this.offsetMatrix, this.offsetMatrix, this.inverseBindpose); 

        for(var j = 0; j < 16; j++){
            bufferMatrix[(offset * 16) + j] = this.offsetMatrix[j];
            // bufferMatrix[(offset * 16) + j] = this.transform.mat4[j];
        }

        offset++;

        // Recursive
        if(recursive){
            for(var i = 0; i < this.children.length; i++){
                offset = this.children[i].calcMatrix(this.offsetMatrix, bufferMatrix, offset, true);
            }
        }

        return offset;
    }

    setAnimation(animation, recursive = true){
        // Recursive first, in case the child bones have the animation, but this one doesn't
        if(recursive){
            for(var i = 0; i < this.children.length; i++){
                this.children[i].setAnimation(animation, recursive);
            }
        }

        // In the case where we don't find anything. We'll want to disable any current animations.
        this.currentAnimation = null;

        // Find this bone in the animation data
        for(var i = 0; i < animation.keyframes.length; i++){
            if(animation.keyframes[i].bone === this){
                this.currentAnimation = animation.keyframes[i]; //TODO: keyframes.keyframes doesn't sound like very intuitive data structure
                break;
            }
        } 
        
        // Nothing found
        if(!this.currentAnimation) return;

        this.currentAnimationTime = 0;
        this.currentFrame = this.currentAnimation.keyframes[0];
        this.currentFrameIndex = 0;
        this.nextFrame = this.currentAnimation.keyframes[1];
        this.nextFrameIndex = 1;

        // Set to frame 0 position
        this.update(0);
    }
}