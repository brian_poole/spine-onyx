'use strict';

import { mat4, vec3, vec4, quat } from '../lib/gl-matrix-master/src/gl-matrix';

let tMat4 = mat4.create();
let tVec3 = vec3.create();

export default class Transform {
	get x() { return this.translationVec3[0]; }
	get y() { return this.translationVec3[1]; }
	get z() { return this.translationVec3[2]; }
	set x(val) { return this.translateToX(val); }
	set y(val) { return this.translateToY(val); }
	set z(val) { return this.translateToZ(val); }
	
	get sx() { return this.scaleVec3[0]; }
	get sy() { return this.scaleVec3[1]; }
	get sz() { return this.scaleVec3[2]; }
	set sx(val) { return this.scaleToX(val); }
	set sy(val) { return this.scaleToY(val); }
	set sz(val) { return this.scaleToZ(val); }
	
	get inverse() {
		mat4.invert(tMat4, this.mat4);
		
		return tMat4;
	}

	get inverseTransposed() {
		mat4.invert(tMat4, this.mat4);
		mat4.transpose(tMat4, tMat4);

		return tMat4;
	}

	getInverseParallax(camera) {
		mat4.invert(tMat4, this.getParallaxMat4(camera));
		
		return tMat4;
	}

	getParallaxMat4(camera) {
		this.fromTranslationRotationScale(tMat4, this.getParallaxOffset(this.translationVec3, camera), this.rotationQuat, this.scaleVec3);
		
		return tMat4;
	}

	getParallaxOffset(v3, camera) {
		vec3.copy(tVec3, v3);

		tVec3[0] -= camera.transform.translationVec3[0] * (tVec3[2] / 100);
		tVec3[1] -= camera.transform.translationVec3[1] * (tVec3[2] / 100);

		return tVec3;
	}
	
	constructor({position = [0,0,0], rotation = [0,0,0,1], scale = [1,1,1]} = {}) {
		this.mat4 = mat4.create();

		this.translationVec3 = vec3.fromValues(position[0] || 0, position[1] || 0, position[2] || 0);
		this.rotationQuat = quat.fromValues(rotation[0] || 0, rotation[1] || 0, rotation[2] || 0, rotation[3] || 1);
		this.scaleVec3 = vec3.fromValues(scale[0] || 1, scale[1] || 1, scale[2] || 1);

		this.move = vec3.create();
		this.move2 = vec3.create();
		this.move3 = vec3.create();
		this.moveQ = quat.create();

		this.refreshMat4();
	}

	generateTranslationRotationScale(){
		mat4.getTranslation(this.translationVec3, this.mat4);
		mat4.getScaling(this.scaleVec3, this.mat4);
		mat4.getRotation(this.rotationQuat, this.mat4);
	}

	refreshMat4() {
		mat4.fromRotationTranslationScale(this.mat4, this.rotationQuat, this.translationVec3, this.scaleVec3);
	}

	fromRotationTranslationScale(rotationQuat = this.rotationQuat, translationVec3 = this.translationVec3, scaleVec3 = this.scaleVec3){
		this.rotationQuat = rotationQuat;
		this.translationVec3 = translationVec3;
		this.scaleVec3 = scaleVec3;

		this.refreshMat4();
	}
	
	translate( x = 0, y = 0, z = 0 ){
		this.translationVec3[0] += x;
		this.translationVec3[1] += y;
		this.translationVec3[2] += z;

		this.refreshMat4();
	}
	
	translateX( x ){
		this.translate(x);
	}
	
	translateY( y ){
		this.translate(0, y);
	}
	
	translateZ( z ){
		this.translate(0, 0, z);
	}
	
	translateTo( x = 0, y = 0, z = 0 ){
		x -= this.translationVec3[0];
		y -= this.translationVec3[1];
		z -= this.translationVec3[2];
		this.translate(x, y, z);
	}
	
	translateToX( x ){
		x -= this.translationVec3[0];
		this.translate(x);
		return this.translationVec3[0];
	}
	
	translateToY( y ){
		y -= this.translationVec3[1];
		this.translate(0, y);
		return this.translationVec3[1];
	}
	
	translateToZ( z ){
		z -= this.translationVec3[2];
		this.translate(0, 0, z);
		return this.translationVec3[2];
	}

	rotateXYZ( radX = 0.0, radY = 0.0, radZ = 0.0 ) {
		quat.rotateX(this.rotationQuat, this.rotationQuat, radX);
		quat.rotateY(this.rotationQuat, this.rotationQuat, radY);
		quat.rotateZ(this.rotationQuat, this.rotationQuat, radZ);
		this.refreshMat4();
	}
	
	rotateZYX( radZ = 0.0, radY = 0.0, radX = 0.0 ) {
		quat.rotateZ(this.rotationQuat, this.rotationQuat, radZ);
		quat.rotateY(this.rotationQuat, this.rotationQuat, radY);
		quat.rotateX(this.rotationQuat, this.rotationQuat, radX);
		this.refreshMat4();
	}
	
	rotateX( rad = 0.0 ) {
		quat.rotateX(this.rotationQuat, this.rotationQuat, rad);
		this.refreshMat4();
	}
	
	rotateY( rad = 0.0 ) {
		quat.rotateY(this.rotationQuat, this.rotationQuat, rad);
		this.refreshMat4();
	}
	
	rotateZ( rad = 0.0 ) {
		quat.rotateZ(this.rotationQuat, this.rotationQuat, rad);
		this.refreshMat4();
	}

	rotateToXYZ( radX = 0.0, radY = 0.0, radZ = 0.0 ){
		this.rotationQuat = quat.create();
		
		quat.rotateX(this.rotationQuat, this.rotationQuat, radX);
		quat.rotateY(this.rotationQuat, this.rotationQuat, radY);
		quat.rotateZ(this.rotationQuat, this.rotationQuat, radZ);
		
		this.refreshMat4();
	}

	rotateToZYX( radZ = 0.0, radY = 0.0, radX = 0.0 ){
		this.rotationQuat = quat.create();
		
		quat.rotateZ(this.rotationQuat, this.rotationQuat, radZ);
		quat.rotateY(this.rotationQuat, this.rotationQuat, radY);
		quat.rotateX(this.rotationQuat, this.rotationQuat, radX);
		
		this.refreshMat4();
	}
	
	rotateToX( rad = 0.0 ) {
        this.rotationQuat = quat.create();
		// TODO: This will clear other 2 axis. Need to fix.
        
		this.rotateX(rad);
	}
	
	rotateToY( rad = 0.0 ) {
        this.rotationQuat = quat.create();
		// TODO: This will clear other 2 axis. Need to fix.
        
		this.rotateY(rad);
	}
	
	rotateToZ( rad = 0.0 ) {
        this.rotationQuat = quat.create();
		// TODO: This will clear other 2 axis. Need to fix.
        
		this.rotateZ(rad);
	}
	
	scale( x=1, y=1, z=1 ) {
		this.scaleVec3[0] *= x;
		this.scaleVec3[1] *= y;
		this.scaleVec3[2] *= z;

		this.refreshMat4();
	}

	scaleTo( x = 1, y = 1, z = 1 ) {
		this.scaleVec3[0] = x;
		this.scaleVec3[1] = y;
		this.scaleVec3[2] = z;

		this.refreshMat4();
	}

	scaleToX( x = 1) {
		this.scaleVec3[0] = x;

		this.refreshMat4();
	}

	scaleToY( y = 1) {
		this.scaleVec3[1] = y;

		this.refreshMat4();
	}

	scaleToZ( z = 1) {
		this.scaleVec3[2] = z;

		this.refreshMat4();
	}
	
	scaleX( x=1 ) {
		this.scale(x);
	}
	
	scaleY( y=1 ) {
		this.scale(1, y);
	}
	
	scaleZ( z=1 ) {
		this.scale(1, 1, z);
	}
	
	lookAt(eye, center, up){
		vec3.set(this.move, eye[0], eye[1], eye[2]);
		vec3.set(this.move2, center[0], center[1], center[2]);
		vec3.set(this.move3, up[0], up[1], up[2]);
		mat4.lookAt(this.mat4, this.move, this.move2, this.move3);
	}
}