'use strict';

import Material from '../material';
import Color from '../color';
import GLProgram from '../gl-program';

const FRAG_SHADER = `
    // #http://www.geeks3d.com/20100228/fog-in-glsl-webgl/
	precision mediump float;

    uniform vec4 fogColor;
    uniform float fogDensity;

    varying vec4 vColor;

    void main(void) {
        // get the distance of the pixel
        float perspective_far = 10000.0;
        float fog_cord = (gl_FragCoord.z / gl_FragCoord.w) / perspective_far;

        // density of fog
        float fog_density = fogDensity;

        // increase fog by density
        // fog also thickens with distance

        float fog = fog_cord * fog_density;

        // mix vertex color with pixel color

        vec4 frag_color = vColor;

        // fog color is white

        vec4 fog_color = fogColor;

        // mix together the frag color with the fog color
        gl_FragColor = mix( fog_color, frag_color, clamp(1.0-fog,0.0,1.0));
    }
`

const VERTEX_SHADER = `
	attribute vec3 aVertexPosition;
	attribute vec4 aVertexColor;

	uniform mat4 uTMatrix;
	uniform mat4 uCMatrix;
	uniform mat4 uMVMatrix;
	uniform mat4 uPMatrix;
	
	varying vec4 vColor;

	void main(void) {
		gl_Position = uPMatrix * uCMatrix * uMVMatrix * uTMatrix * vec4(aVertexPosition, 1.0);
        vColor = aVertexColor;
	}
`

export default class MaterialColoredFog extends Material {
	constructor(vertices = [], vertexColors = [], shaderOptions = {}) {
		super(vertices);
		this.vertexColors = new Float32Array(vertexColors);

		this.cbuffer = null;

		this.vertexShader = VERTEX_SHADER;
		this.fragmentShader = FRAG_SHADER;

		this.shaderAttributes = ["aVertexPosition", "aVertexColor"];
		this.shaderUniformLocations = ["uPMatrix", "uCMatrix", "uMVMatrix", "uTMatrix", "vColor", "fogColor", "fogDensity"];

        this.fogColor = new Color(shaderOptions.fogColor);
        this.fogDensity = shaderOptions.fogDensity || 12.0;
	}

	render( viewport, camera, entity, mesh ) {
		super.render(viewport, camera, entity, mesh);

        // Buffer mesh verts
		if(!this.cbuffer){
			this.cbuffer = viewport.webgl.createBuffer();                             
		}

		viewport.webgl.bindBuffer(viewport.webgl.ARRAY_BUFFER, this.vbuffer);   
		viewport.webgl.bufferData(viewport.webgl.ARRAY_BUFFER, this.vertices, viewport.webgl.DYNAMIC_DRAW);

		// Tell the vertex shader how to read the array
		viewport.webgl.enableVertexAttribArray(viewport.program.attribute.aVertexPosition);
		viewport.webgl.vertexAttribPointer(viewport.program.attribute.aVertexPosition, 3, viewport.webgl.FLOAT, false, 0, 0);
		
		// Buffer vertex colors
		viewport.webgl.bindBuffer(viewport.webgl.ARRAY_BUFFER, this.cbuffer);                                       
		viewport.webgl.bufferData(viewport.webgl.ARRAY_BUFFER, this.vertexColors, viewport.webgl.DYNAMIC_DRAW);

		viewport.webgl.enableVertexAttribArray(viewport.program.attribute.aVertexColor);
        viewport.webgl.vertexAttribPointer(viewport.program.attribute.aVertexColor, 4, viewport.webgl.FLOAT, false, 0, 0);

		// Enable transparency
		viewport.webgl.blendFunc(viewport.webgl.SRC_ALPHA, viewport.webgl.ONE_MINUS_SRC_ALPHA);
		viewport.webgl.enable(viewport.webgl.BLEND);

        // Sends the color to the fragment shader
		viewport.webgl.uniform4fv(viewport.program.uniform.fogColor, this.fogColor.toArray());
        
		viewport.webgl.uniform1f(viewport.program.uniform.fogDensity, this.fogDensity);
		
		viewport.webgl.drawArrays(viewport.webgl.TRIANGLES, 0, this.vertices.length / 3);
	}
}