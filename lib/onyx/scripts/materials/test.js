'use strict';

import Material from '../material';
import Color from '../color';
import GLProgram from '../gl-program';
import Texture from '../texture';

// Variable Qualifiers

// Qualifiers give a special meaning to the variable. The following qualifiers are available:
//		const – The declaration is of a compile time constant.
//		attribute – Global variables that may change per vertex, that are passed from the OpenGL application to vertex shaders. This qualifier can only be used in vertex shaders. For the shader this is a read-only variable. See Attribute section.
//		uniform – Global variables that may change per primitive [...], that are passed from the OpenGL application to the shaders. This qualifier can be used in both vertex and fragment shaders. For the shaders this is a read-only variable. See Uniform section.
//		varying – used for interpolated data between a vertex shader and a fragment shader. Available for writing in the vertex shader, and read-only in a fragment shader. See Varying section.

// The vertex level shader. This is typically the position on screen.
const VERTEX_SHADER = `
	#define SKELETAL_ENABLED
	#define TEXTURES_ENABLED
	#define DIRECTIONAL_LIGHTING_ENABLED

	attribute vec3 aVertexPosition;
	attribute vec3 aVertexNormal;
	attribute vec2 aTextureCoord;
	attribute vec4 aVertexWeights;
	attribute vec4 aVertexWeightIndices;

	uniform mat4 uTMatrix;
	uniform mat4 uCMatrix;
	uniform mat4 uMVMatrix;
	uniform mat4 uPMatrix;
	uniform mat4 uNormalMatrix;
	uniform mat4 uDeformMatrix[64]; // Bone matrices

	uniform vec3 uSceneAmbientLightColor;
	uniform vec3 uDirectionalLightColor;
	uniform vec3 uDirectionalLightVector;
		
	uniform vec3 uMaterialAmbientColor;

	varying highp vec2 vTextureCoord;
	varying highp vec3 vLighting;
	varying highp vec3 vNormal;

	mat4 boneTransform() {
		mat4 ret;

		// Weight normalization factor
		float normfac = 1.0 / (aVertexWeights[0] + aVertexWeights[1] + aVertexWeights[2] + aVertexWeights[3]);

		// Weight1 * Bone1 + Weight2 * Bone2
		ret = normfac * aVertexWeights[0] * uDeformMatrix[int(floor(aVertexWeightIndices[0] + 0.5))]
			+ normfac * aVertexWeights[1] * uDeformMatrix[int(floor(aVertexWeightIndices[1] + 0.5))]
			+ normfac * aVertexWeights[2] * uDeformMatrix[int(floor(aVertexWeightIndices[2] + 0.5))]
			+ normfac * aVertexWeights[3] * uDeformMatrix[int(floor(aVertexWeightIndices[3] + 0.5))];

		return ret;
	}

	void main() {
		// Set the position
		#ifdef SKELETAL_ENABLED
			if(aVertexWeights[0] + aVertexWeights[1] + aVertexWeights[2] + aVertexWeights[3] > 0.0){
				mat4 bt = boneTransform();
				gl_Position = uPMatrix * uCMatrix * uMVMatrix * uTMatrix * bt * vec4(aVertexPosition, 1.0);

				// vVertex = (bt * vec4(aVertex, 1.0)).xyz;
				vNormal = (bt * vec4(aVertexNormal, 0.0)).xyz;
			}else{
				gl_Position = uPMatrix * uCMatrix * uMVMatrix * uTMatrix * vec4(aVertexPosition, 1.0);
				
				vNormal = aVertexNormal;
			}
		#else
			gl_Position = uPMatrix * uCMatrix * uMVMatrix * uTMatrix * vec4(aVertexPosition, 1.0);
			
			vNormal = aVertexNormal;
		#endif
		
        // Set texture coordinate
        #ifdef TEXTURES_ENABLED
            vTextureCoord = aTextureCoord;
        #endif
		
        // Apply lighting effect
        #ifdef DIRECTIONAL_LIGHTING_ENABLED
            highp vec3 directionalVector = normalize(uDirectionalLightVector);

            highp vec4 transformedNormal = uNormalMatrix * vec4(vNormal, 1.0);

            highp float directional = max(dot(normalize(transformedNormal.xyz), directionalVector), 0.0);
            vLighting = uSceneAmbientLightColor + uMaterialAmbientColor + (uDirectionalLightColor * directional);
            // vLighting = vNormal;
        #else
            vLighting = vec3(1.0);
        #endif
	}
`;

// The fragment (or "pixel") level shader.
const FRAG_SHADER = `
#define TEXTURES_ENABLED

precision mediump float;

varying highp vec2 vTextureCoord;
varying highp vec3 vLighting;

uniform sampler2D uSampler;

void main(void) {
    #ifdef TEXTURES_ENABLED
        highp vec4 texelColor = texture2D(uSampler, vTextureCoord);
        
        // Discard if there's no pixel to show. This helps when there's empty space background
        // TODO: Is there a way to optimize this?
        if (texelColor.a == 0.0) 
            discard;
        else
            gl_FragColor = vec4(texelColor.rgb * vLighting, texelColor.a);
    #else
        gl_FragColor = vec4(vLighting, 1.0);
    #endif
}
`;

export default class MaterialPhong extends Material {
	constructor(name = "", {lighting = 1, skeletal = false, ambientTexture = null, diffuseTexture = null, specularColorTexture = null, specularHighlightTexture = null, alphaTexture = null, bumpMapTexture = null, displacementTexture = null, decalTexture = null} = {}) {
		super(name);

		this.vertexShader = VERTEX_SHADER;
		this.fragmentShader = FRAG_SHADER;

		this.shaderAttributes = ["aVertexPosition", "aVertexNormal", "aTextureCoord", "aVertexWeights", "aVertexWeightIndices"];
        this.shaderUniforms = ["uPMatrix", "uCMatrix", "uMVMatrix", "uTMatrix", "uNormalMatrix", "uDeformMatrix",
                                        "uSceneAmbientLightColor", "uDirectionalLightColor", "uDirectionalLightVector",
                                        "uMaterialAmbientColor"//, "uDiffuseColor", "uSpecularColor", "uDiffuse"
                                      ];

        this.ambientColor = new Color(0.0, 0.0, 0.0, 1.0);
        this.diffuseColor = new Color(0.0, 0.0, 0.0, 1.0);
        this.specularColor = new Color(0.0, 0.0, 0.0, 1.0);
        this.diffuse = 1.0;

        this.ambientTexture = ambientTexture;
        this.diffuseTexture = diffuseTexture;
        this.specularColorTexture = specularColorTexture;
        this.specularHighlightTexture = specularHighlightTexture;
        this.alphaTexture = alphaTexture;
        this.bumpMapTexture = bumpMapTexture;
        this.displacementTexture = displacementTexture;
		this.decalTexture = decalTexture;

        this.lighting = lighting;
        this.skeletal = skeletal;
        
		this.buildProgram(); // TODO: Where should "autobuild" this live?
	}

	render(dt, scene, viewport, camera, entity, model, mesh) {
		// Set default uniforms (uPMatrix, uCMatrix, etc...)
        super.render(dt, scene, viewport, camera, entity, model, mesh);
        
        // Set current textures
		if(this.diffuseTexture){
			if(this.diffuseTexture.setAsTexture(viewport, 0) === false) return;
		}

		// Set the lighting
        viewport.webgl.uniform3fv(viewport.program.uniform.uSceneAmbientLightColor, new Float32Array(scene.ambientLightColor.toVec3Array())); //TODO: [Optimization] Prevent creating Float32Arrays every time
        viewport.webgl.uniform3fv(viewport.program.uniform.uDirectionalLightColor, new Float32Array(scene.directionalLightColor.toVec3Array()));
        viewport.webgl.uniform3fv(viewport.program.uniform.uDirectionalLightVector, scene.directionalLightVector);

        viewport.webgl.uniform3fv(viewport.program.uniform.uMaterialAmbientColor, new Float32Array(this.ambientColor.toVec3Array()));

		// Enable transparency
		viewport.webgl.blendFunc(viewport.webgl.SRC_ALPHA, viewport.webgl.ONE_MINUS_SRC_ALPHA);
		viewport.webgl.enable(viewport.webgl.BLEND);
    }

    buildProgram(){
		this.vertexShader = VERTEX_SHADER;
        this.fragmentShader = FRAG_SHADER;
        
        switch(this.lighting){
            case 0:
                this.vertexShader = this.vertexShader.replace("#define DIRECTIONAL_LIGHTING_ENABLED", "");
                break;
        }
		
        if(!this.ambientTexture && !this.diffuseTexture && !this.specularColorTexture && !this.specularHighlightTexture && !this.alphaTexture && !this.bumpMapTexture && !this.displacementTexture && !this.decalTexture){
			this.vertexShader = this.vertexShader.replace("#define TEXTURES_ENABLED", "");
            this.fragmentShader = this.fragmentShader.replace("#define TEXTURES_ENABLED", "");
		}
		
		if(!this.skeletal){
			this.vertexShader = this.vertexShader.replace("#define SKELETAL_ENABLED", "");
		}
    }
}