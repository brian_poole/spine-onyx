'use strict';

import Material from '../material';
import Texture from '../texture';
import TextureCache from '../texture-cache';
import Color from '../color';

//http://stackoverflow.com/a/12335384
const FRAG_SHADER = `
	precision mediump float;

    varying vec2 vTextureCoord;

    uniform sampler2D uSampler;

    void main(void) {
		vec4 textureColor = texture2D(uSampler, vTextureCoord);
		if (textureColor.a < 0.1) 
			discard;
		else
			gl_FragColor = vec4(textureColor.rgb, textureColor.a);
    }
`
// gl_FragColor = texture2D(uSampler, vec2(vTextureCoord.s, vTextureCoord.t));

const VERTEX_SHADER = `
	attribute vec3 aVertexPosition;
	attribute vec2 aTextureCoord;

	uniform mat4 uTMatrix;
	uniform mat4 uCMatrix;
	uniform mat4 uMVMatrix;
	uniform mat4 uPMatrix;
	
	varying vec2 vTextureCoord;

	void main(void) {
		gl_Position = uPMatrix * uCMatrix * uMVMatrix * uTMatrix * vec4(aVertexPosition, 1.0);
		vTextureCoord = aTextureCoord;
	}
`

export default class MaterialTilemap extends Material {
	constructor(vertices = [], textureCoords = [], textureSrc = "") {
		super(vertices);

		this.depthFunc = 'LEQUAL';

		this.textureCoords = new Float32Array(textureCoords);

		this.texture = null;
		this.tbuffer = null;

		this.textureLoad = TextureCache.load(textureSrc).then((texture) => {
			this.texture = texture;
		});

		this.vertexShader = VERTEX_SHADER;
		this.fragmentShader = FRAG_SHADER;

		this.shaderAttributes = ["aVertexPosition", "aTextureCoord"];
		this.shaderUniformLocations = ["uPMatrix", "uCMatrix", "uMVMatrix", "uTMatrix", "uSampler", "uColor"];
	}

	render( viewport, camera, entity, mesh ) {
		super.render(viewport, camera, entity, mesh);
		
		// Skip if texture isn't loaded?
		if(!this.texture) return;

		// Create buffers
		if(!this.tbuffer){
			this.tbuffer = viewport.webgl.createBuffer();
		}

		viewport.webgl.bindBuffer(viewport.webgl.ARRAY_BUFFER, this.vbuffer);   
		viewport.webgl.bufferData(viewport.webgl.ARRAY_BUFFER, this.vertices, viewport.webgl.DYNAMIC_DRAW);

		// Tell the vertex shader how to read the array
		viewport.webgl.enableVertexAttribArray(viewport.program.attribute.aVertexPosition);
		viewport.webgl.vertexAttribPointer(viewport.program.attribute.aVertexPosition, 3, viewport.webgl.FLOAT, false, 0, 0);
		
		// Set current texture
		if(this.texture){
			if(this.texture.setAsTexture(viewport) === false) return;
		}
		
		// Buffer texture verts
		viewport.webgl.bindBuffer(viewport.webgl.ARRAY_BUFFER, this.tbuffer);                                       
		viewport.webgl.bufferData(viewport.webgl.ARRAY_BUFFER, this.textureCoords, viewport.webgl.DYNAMIC_DRAW);

		viewport.webgl.enableVertexAttribArray(viewport.program.attribute.aTextureCoord);
        viewport.webgl.vertexAttribPointer(viewport.program.attribute.aTextureCoord, 2, viewport.webgl.FLOAT, false, 0, 0);

		// Enable transparency
		viewport.webgl.blendFunc(viewport.webgl.SRC_ALPHA, viewport.webgl.ONE_MINUS_SRC_ALPHA);
		viewport.webgl.enable(viewport.webgl.BLEND);
		
		viewport.webgl.drawArrays(viewport.webgl.TRIANGLES, 0, this.vertices.length / 3);
	}
}