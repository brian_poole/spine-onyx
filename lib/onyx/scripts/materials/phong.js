'use strict';

import Material from '../material';
import Color from '../color';
import Texture from '../texture';

const BONE_MAX = 4;

// Variable Qualifiers

// Qualifiers give a special meaning to the variable. The following qualifiers are available:
//		const – The declaration is of a compile time constant.
//		attribute – Global variables that may change per vertex, that are passed from the OpenGL application to vertex shaders. This qualifier can only be used in vertex shaders. For the shader this is a read-only variable. See Attribute section.
//		uniform – Global variables that may change per primitive [...], that are passed from the OpenGL application to the shaders. This qualifier can be used in both vertex and fragment shaders. For the shaders this is a read-only variable. See Uniform section.
//		varying – used for interpolated data between a vertex shader and a fragment shader. Available for writing in the vertex shader, and read-only in a fragment shader. See Varying section.

// The vertex level shader. This is typically the position on screen.
const VERTEX_SHADER = `
	#define SKELETAL_ENABLED
	#define TEXTURES_ENABLED
	#define DIRECTIONAL_LIGHTING_ENABLED

	attribute vec3 aVertexPosition;
	attribute vec3 aVertexNormal;
	attribute vec2 aTextureCoord;
	attribute vec4 aVertexWeights;
	attribute vec4 aVertexWeightIndices;

	uniform mat4 uMVCPMatrix; // Model View Camera Projection Matrix
	uniform mat4 uMMatrix; // Model
	uniform mat4 uTMatrix; // Entity
	uniform mat4 uCMatrix; // Camera
	uniform mat4 uMVMatrix;// Inverse Camera
	uniform mat4 uPMatrix; // Projection
	uniform mat4 uNormalMatrix;
	uniform mat4 uDeformMatrix[${BONE_MAX}]; // Bone matrices

	uniform vec3 uSceneAmbientLightColor;
	uniform vec3 uDirectionalLightColor;
	uniform vec3 uDirectionalLightVector;
		
	uniform vec3 uMaterialAmbientColor;
	uniform vec4 uFogColor;
	uniform float uFogDensity;
	uniform float uFogDistance;
	
	varying vec4 vPositionCoord;
	varying vec2 vTextureCoord;
	varying vec3 vLighting;
	varying vec3 vNormal;
	varying vec4 vFogColor;
	varying float vFogDensity;
	varying float vFogDistance;
	
	mat4 boneTransformMat4() {
		mat4 ret;

		// Weight normalization factor
		float normfac = 1.0 / (aVertexWeights[0] + aVertexWeights[1] + aVertexWeights[2] + aVertexWeights[3]);

		// Weight1 * Bone1 + Weight2 * Bone2
		ret = normfac * aVertexWeights[0] * uDeformMatrix[int(floor(aVertexWeightIndices[0] + 0.5))]
			+ normfac * aVertexWeights[1] * uDeformMatrix[int(floor(aVertexWeightIndices[1] + 0.5))]
			+ normfac * aVertexWeights[2] * uDeformMatrix[int(floor(aVertexWeightIndices[2] + 0.5))]
			+ normfac * aVertexWeights[3] * uDeformMatrix[int(floor(aVertexWeightIndices[3] + 0.5))];

		return ret;
	}

	vec4 boneTransform(vec4 aVert) {
		// Weight normalization factor
		float normfac = 1.0 / (aVertexWeights[0] + aVertexWeights[1] + aVertexWeights[2] + aVertexWeights[3]);

		// Weight1 * Bone1 + Weight2 * Bone2
		vec4 v = vec4(0.0);
		v += uDeformMatrix[int(floor(aVertexWeightIndices[0] + 0.5))] * aVert * aVertexWeights[0];
		v += uDeformMatrix[int(floor(aVertexWeightIndices[1] + 0.5))] * aVert * aVertexWeights[1];
		v += uDeformMatrix[int(floor(aVertexWeightIndices[2] + 0.5))] * aVert * aVertexWeights[2];
		v += uDeformMatrix[int(floor(aVertexWeightIndices[3] + 0.5))] * aVert * aVertexWeights[3];

		return v;
	}

	void main() {
		vFogColor = uFogColor;
		vFogDensity = uFogDensity;
		vFogDistance = uFogDistance;
		
		// Set the position
		#ifdef SKELETAL_ENABLED
			if(aVertexWeights[0] + aVertexWeights[1] + aVertexWeights[2] + aVertexWeights[3] > 0.0){
				// mat4 bt = boneTransform();
				// gl_Position = uMVCPMatrix * bt * vec4(aVertexPosition, 1.0);

				// // vVertex = (bt * vec4(aVertex, 1.0)).xyz;
				// vNormal = (bt * vec4(aVertexNormal, 0.0)).xyz;

				gl_Position = uMVCPMatrix * boneTransform(vec4(aVertexPosition, 1.0));
				vNormal = boneTransform(vec4(aVertexNormal, 1.0)).xyz;
			}else{
				gl_Position = uMVCPMatrix * vec4(aVertexPosition, 1.0);
				
				vNormal = aVertexNormal;
			}
		#else
			gl_Position = uMVCPMatrix * vec4(aVertexPosition, 1.0);
			
			vNormal = aVertexNormal;
		#endif
		vPositionCoord = uMVMatrix * uTMatrix * uMMatrix * vec4(aVertexPosition, 1.0);
		
        // Set texture coordinate
        #ifdef TEXTURES_ENABLED
            vTextureCoord = aTextureCoord;
        #endif
		
        // Apply lighting effect
        #ifdef DIRECTIONAL_LIGHTING_ENABLED
            vec3 directionalVector = normalize(uDirectionalLightVector);

            vec4 transformedNormal = uNormalMatrix * vec4(vNormal, 1.0);

            float directional = max(dot(normalize(transformedNormal.xyz), directionalVector), 0.0);
            vLighting = uSceneAmbientLightColor + uMaterialAmbientColor + (uDirectionalLightColor * directional);
            // vLighting = vNormal;
        #else
            vLighting = vec3(1.0);
        #endif
	}
`;

// The fragment (or "pixel") level shader.
const FRAG_SHADER = `
	#define FOG_ENABLED
	#define TEXTURE_DIFFUSE_ENABLED
	#define TEXTURE_AMBIENT_ENABLED
	#define TEXTURE_AMBIENT_BLEND_MODE 1 // 0: Additive | 1: Multiply | 2: Screen
	#define TEXTURE_AMBIENT_COORDINATES 0 // 0: Use UVs | 1: Use World Coordinates XY | 2: Use World Coordinates XZ
	#define TEXTURE_AMBIENT_SIZE 1024.00
	#define TEXTURE_AMBIENT_SCALE 1024.00

	precision mediump float;

	varying vec4 vPositionCoord;
	varying vec2 vTextureCoord;
	varying vec3 vLighting;
	varying vec4 vFogColor;
	varying float vFogDensity;
	varying float vFogDistance;
	
	uniform sampler2D uDiffuseTexture;
	uniform sampler2D uAmbientTexture;

	void main(void) {
	 vec4 texelColor = vec4(1.0, 1.0, 1.0, 1.0);

		#ifdef TEXTURE_DIFFUSE_ENABLED
			texelColor *= texture2D(uDiffuseTexture, vTextureCoord);
		#endif
		#ifdef TEXTURE_AMBIENT_ENABLED
			#if TEXTURE_AMBIENT_COORDINATES == 0
				texelColor *= texture2D(uAmbientTexture, vTextureCoord * vec2(1,-1)); //TODO: Should we be flipping Y here?
			#elif TEXTURE_AMBIENT_COORDINATES == 1
				texelColor *= texture2D(uAmbientTexture, vPositionCoord.xy / (TEXTURE_AMBIENT_SIZE * TEXTURE_AMBIENT_SCALE));
			#elif TEXTURE_AMBIENT_COORDINATES == 2
				// texelColor += texture2D(uAmbientTexture, vPositionCoord.xz / TEXTURE_AMBIENT_SIZE);
				texelColor *= texture2D(uAmbientTexture, vPositionCoord.xz / (TEXTURE_AMBIENT_SIZE * TEXTURE_AMBIENT_SCALE));
				//texelColor = 1.0 - (1.0 - texelColor) * (1.0 - texture2D(uAmbientTexture, vPositionCoord.xz / TEXTURE_AMBIENT_SIZE));
			#endif
		#endif

		// Discard if there's no pixel to show. This helps when there's empty space background
		// TODO: Is there a way to optimize this?
		if (texelColor.a == 0.0) 
			discard;
		else
			gl_FragColor = texelColor * vec4(vLighting, 1.0);

		#ifdef FOG_ENABLED
			// get the distance of the pixel
			float perspective_far = vFogDistance;
			float fog_cord = (gl_FragCoord.z / gl_FragCoord.w) / perspective_far;

			// density of fog
			float fog_density = vFogDensity;

			// increase fog by density
			// fog also thickens with distance

			float fog = fog_cord * fog_density;

			// mix vertex color with pixel color

			vec4 frag_color = gl_FragColor;

			// fog color is white

			vec4 fog_color = vFogColor;

			// mix together the frag color with the fog color
			gl_FragColor = mix( fog_color, frag_color, clamp(1.0-fog,0.0,1.0));
		#endif
	}
`;

const ATTRIBUTES = ["aVertexPosition", "aVertexNormal", "aTextureCoord", "aVertexWeights", "aVertexWeightIndices"];

const UNIFORMS = ["uMVCPMatrix", "uPMatrix", "uCMatrix", "uMVMatrix", "uTMatrix", "uMMatrix", "uNormalMatrix", "uDeformMatrix",
						"uSceneAmbientLightColor", "uDirectionalLightColor", "uDirectionalLightVector",
						"uMaterialAmbientColor",// "uDiffuseColor", "uSpecularColor", "uDiffuse",
						"uFogColor", "uFogDensity", "uFogDistance",
						"uDiffuseTexture", "uAmbientTexture"
						];

export default class MaterialPhong extends Material {
	constructor(name = "", {lighting = 1, skeletal = false, ambientTextureCoordinateSpace = "UVtoXZ", receivesFog = true, ambientTexture = null, diffuseTexture = null, specularColorTexture = null, specularHighlightTexture = null, alphaTexture = null, bumpMapTexture = null, displacementTexture = null, decalTexture = null, cullFace = false, cullFaceMode = "BACK", frontFace = "CW"} = {}) {
		super(name);

		this.vertexShader = VERTEX_SHADER;
		this.fragmentShader = FRAG_SHADER;

		this.shaderAttributes = [...ATTRIBUTES];
        this.shaderUniforms = [...UNIFORMS];

        this.ambientColor = new Color(0.0, 0.0, 0.0, 1.0);
        this.diffuseColor = new Color(0.0, 0.0, 0.0, 1.0);
        this.specularColor = new Color(0.0, 0.0, 0.0, 1.0);
        this.diffuse = 1.0;

        this.ambientTexture = ambientTexture;
        this.diffuseTexture = diffuseTexture;
        this.specularColorTexture = specularColorTexture;
        this.specularHighlightTexture = specularHighlightTexture;
        this.alphaTexture = alphaTexture;
        this.bumpMapTexture = bumpMapTexture;
        this.displacementTexture = displacementTexture;
		this.decalTexture = decalTexture;

        this.lighting = lighting;
		this.skeletal = skeletal; // TODO Rename to "skinning"?

		this.cullFace = cullFace;
		this.cullFaceMode = cullFaceMode;
		this.frontFace = frontFace;

		this.receivesFog = receivesFog;
		
		this.ambientTextureCoordinateSpace = ambientTextureCoordinateSpace; // TODO: Name this var better?
        
		this.buildProgram(); // TODO: Where should "autobuild" this live?
	}

	render(dt, scene, viewport, camera, entity, model, mesh) {
		// Set default uniforms (uPMatrix, uCMatrix, etc...)
        super.render(dt, scene, viewport, camera, entity, model, mesh);
        
        // Set current textures
		if(this.diffuseTexture){
			if(this.diffuseTexture.setAsTexture(viewport, 0, viewport.program.uniform.uDiffuseTexture) === false) return;
		}

		if(this.ambientTexture){
			if(this.ambientTexture.setAsTexture(viewport, 1, viewport.program.uniform.uAmbientTexture) === false) return;
		}

		if(this.cullFace){
			viewport.webgl.enable(viewport.webgl.CULL_FACE);
			viewport.webgl.cullFace(viewport.webgl[this.cullFaceMode]);
			viewport.webgl.frontFace(viewport.webgl[this.frontFace]);
		}else{
			viewport.webgl.disable(viewport.webgl.CULL_FACE);
		}

		// Set the lighting
        viewport.webgl.uniform3fv(viewport.program.uniform.uSceneAmbientLightColor, new Float32Array(scene.ambientLightColor.toVec3Array())); //TODO: [Optimization] Prevent creating Float32Arrays every time
        viewport.webgl.uniform3fv(viewport.program.uniform.uDirectionalLightColor, new Float32Array(scene.directionalLightColor.toVec3Array()));
		viewport.webgl.uniform3fv(viewport.program.uniform.uDirectionalLightVector, scene.directionalLightVector);

		viewport.webgl.uniform3fv(viewport.program.uniform.uMaterialAmbientColor, new Float32Array(this.ambientColor.toVec3Array()));
		viewport.webgl.uniform4fv(viewport.program.uniform.uFogColor, new Float32Array(scene.fogColor.toArray()));
		viewport.webgl.uniform1f(viewport.program.uniform.uFogDensity, scene.fogDensity);
		viewport.webgl.uniform1f(viewport.program.uniform.uFogDistance, viewport._farPlane);
		
		// Enable transparency
		viewport.webgl.blendFunc(viewport.webgl.SRC_ALPHA, viewport.webgl.ONE_MINUS_SRC_ALPHA);
		viewport.webgl.enable(viewport.webgl.BLEND);
    }

    buildProgram(){
		this.shaderAttributes = [...ATTRIBUTES];
		this.shaderUniforms = [...UNIFORMS];
		
		this.vertexShader = VERTEX_SHADER;
        this.fragmentShader = FRAG_SHADER;
        
        switch(this.lighting){
			case 1:
				// Leave it alone
				break;
			default:
                this.vertexShader = this.vertexShader.replace("#define DIRECTIONAL_LIGHTING_ENABLED", "");
                break;
        }
		
        if(!this.ambientTexture && !this.diffuseTexture && !this.specularColorTexture && !this.specularHighlightTexture && !this.alphaTexture && !this.bumpMapTexture && !this.displacementTexture && !this.decalTexture){
			this.vertexShader = this.vertexShader.replace("#define TEXTURES_ENABLED", "");
            this.fragmentShader = this.fragmentShader.replace("#define TEXTURES_ENABLED", "");
		}

		if(!this.ambientTexture){
			this.fragmentShader = this.fragmentShader.replace("#define TEXTURE_AMBIENT_ENABLED", "");
		}else{
			this.fragmentShader = this.fragmentShader.replace("#define TEXTURE_AMBIENT_SIZE 1024.00", "#define TEXTURE_AMBIENT_SIZE " + this.ambientTexture.width.toFixed(2));
			this.fragmentShader = this.fragmentShader.replace("#define TEXTURE_AMBIENT_SCALE 1024.00", "#define TEXTURE_AMBIENT_SCALE " + this.ambientTexture.scale.toFixed(2));
			
			switch(this.ambientTextureCoordinateSpace){
				case "UV":
					this.fragmentShader = this.fragmentShader.replace("#define TEXTURE_AMBIENT_COORDINATES 0", "#define TEXTURE_AMBIENT_COORDINATES 0");
					break;
				case "UVtoXY":
					this.fragmentShader = this.fragmentShader.replace("#define TEXTURE_AMBIENT_COORDINATES 0", "#define TEXTURE_AMBIENT_COORDINATES 1");
					break;
				case "UVtoXZ":
					this.fragmentShader = this.fragmentShader.replace("#define TEXTURE_AMBIENT_COORDINATES 0", "#define TEXTURE_AMBIENT_COORDINATES 2");
					break;
			}
			if(this.ambientTextureCoordinateSpace !== "WORLD"){
				this.fragmentShader = this.fragmentShader.replace("#define TEXTURE_AMBIENT_COORDINATES 1", "#define TEXTURE_AMBIENT_COORDINATES 0");
			}
		}

		if(!this.diffuseTexture){
			this.fragmentShader = this.fragmentShader.replace("#define TEXTURE_DIFFUSE_ENABLED", "");
		}
		
		if(!this.skeletal){
			this.vertexShader = this.vertexShader.replace("#define SKELETAL_ENABLED", "");

			// Because Edge doesn't optimize uniforms out automatically
			if(this.shaderUniforms.indexOf("uDeformMatrix") > -1) this.shaderUniforms.splice(this.shaderUniforms.indexOf("uDeformMatrix"), 1);
		}

		if(!this.receivesFog){
			this.fragmentShader = this.fragmentShader.replace("#define FOG_ENABLED", "");
		}
    }
}