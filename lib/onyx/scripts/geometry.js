'use strict';

import Component from './component';

import MaterialPhong from './materials/phong';
import Mesh from './mesh';
import SpriteAnimation from './sprite-animation';
import Texture from './texture';
import Transform from './transform';

const FACE = [
	0.0, 0.0, 0.0,
	1.0, 0.0, 0.0,
	1.0, 1.0, 0.0,
	0.0, 1.0, 0.0,
	0.0, 0.0, 0.0,
	1.0, 1.0, 0.0 
];
            
const FACE_UVS = [
	0.0, 1.0, //  left,    top, 
	1.0, 1.0, // right,    top, 
	1.0, 0.0, // right, bottom, 
	0.0, 0.0, //  left, bottom, 
	0.0, 1.0, //  left,    top, 
	1.0, 0.0  // right, bottom, 
];

const FACE_NORMALS = [
	0.0, 0.0, 1.0,
	0.0, 0.0, 1.0,
	0.0, 0.0, 1.0,
	0.0, 0.0, 1.0,
	0.0, 0.0, 1.0,
	0.0, 0.0, 1.0
];

export default class Geometry {
	constructor(src, {transform = new Transform()} = {}) {

	}

	loadTextures(){
		return this.texture.load();
	}
	
	render(dt, scene, viewport, camera, entity){
		super.render(dt, scene, viewport, camera, entity);

		this.mesh.render(dt, scene, viewport, camera, entity, this);
	}
	
	update(dt, scene, entity){
		super.update(dt, scene, entity);

		if(this.currentAnimation) this.currentAnimation.update(dt, scene, entity, this);
	}

	addAnimation(animationId, animation){
		this.animations[animationId] = animation;

		//TODO: This part feels a little hacky
		animation.sprite = this;
	}

	playAnimation(animationId, callback){
		if(!this.animations[animationId]) return console.warn(`Animation '${animationId}' not found`);

		this.currentAnimation = this.animations[animationId];
		this.currentAnimation.play(callback);
	}

	selectAnimation(animationId){
		if(!this.animations[animationId]) return console.warn(`Animation '${animationId}' not found`);

		this.currentAnimation = this.animations[animationId];
		this.currentAnimation.setFrame(0);
	}

	selectFrame(x = 1, y = 1, width = 0, height = 0, pivotX = 0, pivotY = 0, materialIndex = 0){
		let material = this.mesh.material//s[materialIndex];
		let texture = material.diffuseTexture;
		if(texture === null) return;

		let left = x / texture.img.width;
		let right = (x + width) / texture.img.width;
		let bottom = y / texture.img.height;
		let top = (y + height) / texture.img.height;
		
		if(this.flipX) [left, right] = [right, left];
		if(this.flipY) [top, bottom] = [bottom, top];

		this.mesh.textureVertices.set([
			 left,    top, 
			right,    top, 
			right, bottom, 
			 left, bottom, 
			 left,    top, 
			right, bottom
		]);

		this.mesh.invalidate(false, true);

		if(pivotX !== this.pivot[0] || pivotX !== this.pivot[1]){
			let offsetX = width * pivotX;
			let offsetY = height * pivotY;

			this.mesh.vertices[0] = -offsetX;
			this.mesh.vertices[3] =  offsetX;
			this.mesh.vertices[6] =  offsetX;
			this.mesh.vertices[9] = -offsetX;
			this.mesh.vertices[12] = -offsetX;
			this.mesh.vertices[15] =  offsetX;
			
			this.mesh.vertices[1] = -offsetY;
			this.mesh.vertices[4] = -offsetY;
			this.mesh.vertices[7] =  offsetY;
			this.mesh.vertices[10] =  offsetY;
			this.mesh.vertices[13] = -offsetY;
			this.mesh.vertices[16] =  offsetY;
		}
	}
}