'use strict';

export default class SpriteAnimation {
	constructor({loop = true} = {}) {
		this.frames = [];
		
		this.state = 0;
		this.loop = loop;
		
		this.currentTime = 0;
		this.currentFrame = null;
		this.currentFrameIndex = 0;

		this.callback = null;
	}
	
	update(dt, scene, entity, sprite){
		if(!this.state) return;
		let speed = this.currentFrame.speed || 0;
		
		if(this.currentTime + dt > speed){
			// Advance frame index
			this.currentFrameIndex++;
			if(!this.frames[this.currentFrameIndex]){
				if(this.callback) this.callback();
				
				if(this.loop){
					this.currentFrameIndex = 0;
				}else{
					this.state = 0;
					return;
				}
			}
			
			// Assign frame
			this.setFrame();
			
			// Clear
			this.currentTime = 0;
		}
		
		this.currentTime += dt;
	}
	
	play(callback = null, loop = true){
		this.state = 1;
		this.loop = loop;
		this.callback = callback;
		this.reset();
		this.setFrame();
	}
	
	reset(){
		this.currentTime = 0;
		this.currentFrame = this.frames[0];
		this.currentFrameIndex = 0;
	}

	addFrame({x = 0, y = 0, width = 0, height = 0, pivotX = 0, pivotY = 0, speed = 0} = {}){
		this.frames.push({x: x, y: y, width: width, height: height, pivotX: pivotX, pivotY: pivotY, speed: speed});
	}

	setFrame(frameIndex = this.currentFrameIndex){
		this.currentFrame = this.frames[this.currentFrameIndex];
		
		// TODO: "this.sprite" feels a little hacky
		this.sprite.selectFrame(this.currentFrame.x, this.currentFrame.y, this.currentFrame.width, this.currentFrame.height, this.currentFrame.pivotX, this.currentFrame.pivotY, this.currentFrame.materialIndex);
	}
}