'use strict';

import ShaderPass from './shaderpass';

export default class Composer {
	constructor(passes = []) {
		this.passes = passes;
	}

	render( camera, viewport ) {
		for(let i = 0; i < this.passes; i++){
			this.passes.render( this, camera, viewport );
		}
	}
}