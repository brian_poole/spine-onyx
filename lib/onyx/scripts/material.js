'use strict';

import { mat4 } from '../lib/gl-matrix-master/src/gl-matrix';

import Texture from './texture';
import Color from './color';

let tMatrix = mat4.create();

export default class Material {
	constructor(name = "") {
		this.name = name;
		this.program = null;
		this.vbuffer = null;

		this.depthWrite = true;
		this.depthTest = true;
		this.depthFunc = 'LEQUAL';
	}

	render(dt, scene, viewport, camera, entity, model, mesh) {
		// TODO: We should compile a shader cache, so that way we aren't unnecessarily swapping shaders
		// TODO: Compile per viewport
		if(!this.program){
			this.program = viewport.createProgram(this);
		}
		viewport.useProgram(this.program);

		// Enable depth testing
		if(this.depthTest){
			viewport.webgl.enable(viewport.webgl.DEPTH_TEST);
			viewport.webgl.depthFunc(viewport.webgl[this.depthFunc]);
		}else{
			viewport.webgl.disable(viewport.webgl.DEPTH_TEST);
		}

		viewport.webgl.blendEquationSeparate( viewport.webgl.FUNC_ADD, viewport.webgl.FUNC_ADD );
		viewport.webgl.blendFuncSeparate( viewport.webgl.SRC_ALPHA, viewport.webgl.ONE_MINUS_SRC_ALPHA, viewport.webgl.ONE, viewport.webgl.ONE_MINUS_SRC_ALPHA );

		if(viewport.program.uniform.uMVCPMatrix !== undefined){
			// uPMatrix * uCMatrix * uMVMatrix * uTMatrix * uMMatrix
			mat4.mul(tMatrix, viewport.projectionMatrix, camera.transform.inverse);
			mat4.mul(tMatrix, tMatrix, camera.mvTransform.mat4);
			mat4.mul(tMatrix, tMatrix, entity.transform.mat4);
			mat4.mul(tMatrix, tMatrix, model.transform.mat4);
			
			viewport.webgl.uniformMatrix4fv(viewport.program.uniform.uMVCPMatrix, false, tMatrix);
		}
		
		// Set the camera position matrix
		viewport.webgl.uniformMatrix4fv(viewport.program.uniform.uCMatrix, false, camera.transform.inverse);
		
		// Set the model view matrix (Inverse of camera position)
		viewport.webgl.uniformMatrix4fv(viewport.program.uniform.uMVMatrix, false, camera.mvTransform.mat4);

		// Set the model matrix
		if(viewport.program.uniform.uMMatrix !== undefined){
			// TODO: Can this be optimized?
			// Note: The second argument (transpose) MUST be false according to WebGL spec
			viewport.webgl.uniformMatrix4fv(viewport.program.uniform.uMMatrix, false, model.transform.mat4); 
		}

		if(viewport.program.uniform.uNormalMatrix !== undefined){
			// TODO: Can this be optimized?
			// Note: The second argument (transpose) MUST be false according to WebGL spec
			viewport.webgl.uniformMatrix4fv(viewport.program.uniform.uNormalMatrix, false, entity.transform.inverseTransposed); 
		}

		// Set the entity position matrix
		if(viewport.parallax){
			viewport.webgl.uniformMatrix4fv(viewport.program.uniform.uTMatrix, false, entity.transform.getParallaxMat4(camera));
		}else{
			viewport.webgl.uniformMatrix4fv(viewport.program.uniform.uTMatrix, false, entity.transform.mat4);
		}
	}

	setValues(obj){
		return;
	}
}