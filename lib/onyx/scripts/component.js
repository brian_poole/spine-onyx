'use strict';

export default class Component {
	constructor(){
		this.enabled = true;
		this.visible = true;
	}
	
	update(dt, scene, entity){
		
	}
	
	render(dt, scene, viewport, camera, entity){
		
	}
}