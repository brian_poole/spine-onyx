'use strict';

export default class SkeletalAnimation {
	constructor({name = "", fps = 30, keyframes = [], length = 0} = {}) {
        this.name = name;
        this.fps = fps;
        this.keyframes = keyframes;
        this.length = length;
    }
    
    update(){
        
    }
}