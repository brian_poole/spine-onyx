'use strict';

import Component from './component';

export default class Mesh extends Component {
	constructor({material = null, vertices = new Float32Array(0), uvs = new Float32Array(0), normals = new Float32Array(0), weights = new Float32Array(0), weightIndices = new Float32Array(0), parameters = new Float32Array(0), faces = new Float32Array(0), indices = new Float32Array(0)} = {}) {
        super();

		this.material = material; // TODO: Should we have multiple materials?
		this.vertices = vertices;
		this.uvs = uvs;
		this.normals = normals;
		this.indices = indices;
		// this.parameters = parameters;
		// this.faces = faces;
		this.weights = weights;
		this.weightIndices = weightIndices;

		this.castShadow = true;
		this.receiveShadow = true;

		this.buffers = { // TODO: Make this an array per viewport
			vertices: null,
			uvs: null,
			normals: null,
			indices: null,
			weights: null,
			weightIndices: null
		}

		this.valid = { // TODO: Make this an array per viewport
			vertices: false,
			uvs: false,
			normals: false,
			indices: false,
			weights: false,
			weightIndices: false
		};
	}

	update() {
		// Anything?
	}

	render(dt, scene, viewport, camera, entity, model){
		if(!this.material || !this.vertices.length) return;
		
		this.material.render(dt, scene, viewport, camera, entity, model, this);

		// Buffer mesh verts
		if(viewport.program.attribute.aVertexPosition !== undefined){
			if(!this.buffers.vertices || !this.valid.vertices){
				this.buffers.vertices = viewport.webgl.createBuffer();        

				// Pass vertices to buffer
				// TODO: Do this once per vertices being updated (but bind on every frame)
				viewport.webgl.bindBuffer(viewport.webgl.ARRAY_BUFFER, this.buffers.vertices);   
				viewport.webgl.bufferData(viewport.webgl.ARRAY_BUFFER, this.vertices, viewport.webgl.STATIC_DRAW); // TODO: Allow mesh to define type?
				
				this.valid.vertices = true;
			}
			viewport.webgl.bindBuffer(viewport.webgl.ARRAY_BUFFER, this.buffers.vertices);   

			// Tell the vertex shader how to read the array
			viewport.webgl.vertexAttribPointer(viewport.program.attribute.aVertexPosition, 3, viewport.webgl.FLOAT, false, 0, 0);
			viewport.webgl.enableVertexAttribArray(viewport.program.attribute.aVertexPosition);
		}

		// UVs
		if(viewport.program.attribute.aTextureCoord !== undefined){
			// Buffer mesh texture coordinates
			if(!this.buffers.uvs || !this.valid.uvs){
				this.buffers.uvs = viewport.webgl.createBuffer();             

				// Pass texture coordinates to buffer
				viewport.webgl.bindBuffer(viewport.webgl.ARRAY_BUFFER, this.buffers.uvs);   
				viewport.webgl.bufferData(viewport.webgl.ARRAY_BUFFER, this.uvs, viewport.webgl.STATIC_DRAW);        
				
				this.valid.uvs = true; 
			}
			viewport.webgl.bindBuffer(viewport.webgl.ARRAY_BUFFER, this.buffers.uvs);   
			
			// Tell the vertex shader how to read the array
			viewport.webgl.vertexAttribPointer(viewport.program.attribute.aTextureCoord, 2, viewport.webgl.FLOAT, false, 0, 0);
			viewport.webgl.enableVertexAttribArray(viewport.program.attribute.aTextureCoord);
		}
		
		// Normals
		if(viewport.program.attribute.aVertexNormal !== undefined){
			// Buffer mesh normals
			if(!this.buffers.normals || !this.valid.normals){
				this.buffers.normals = viewport.webgl.createBuffer();             

				// Pass normals to buffer
				viewport.webgl.bindBuffer(viewport.webgl.ARRAY_BUFFER, this.buffers.normals);   
				viewport.webgl.bufferData(viewport.webgl.ARRAY_BUFFER, this.normals, viewport.webgl.STATIC_DRAW);         
				
				this.valid.normals = true;
			}
			viewport.webgl.bindBuffer(viewport.webgl.ARRAY_BUFFER, this.buffers.normals);   
			
			// Tell the vertex shader how to read the array
			viewport.webgl.vertexAttribPointer(viewport.program.attribute.aVertexNormal, 3, viewport.webgl.FLOAT, false, 0, 0);
			viewport.webgl.enableVertexAttribArray(viewport.program.attribute.aVertexNormal);
		}

		// Weights
		if(viewport.program.attribute.aVertexWeights !== undefined){
			// Buffer mesh weights
			if(!this.buffers.weights || !this.valid.weights){
				this.buffers.weights = viewport.webgl.createBuffer();             

				// Pass weights to buffer
				viewport.webgl.bindBuffer(viewport.webgl.ARRAY_BUFFER, this.buffers.weights);   
				viewport.webgl.bufferData(viewport.webgl.ARRAY_BUFFER, this.weights, viewport.webgl.STATIC_DRAW);         
				
				this.valid.weights = true;
			}
			viewport.webgl.bindBuffer(viewport.webgl.ARRAY_BUFFER, this.buffers.weights);   
			
			// Tell the vertex shader how to read the array
			viewport.webgl.vertexAttribPointer(viewport.program.attribute.aVertexWeights, 4, viewport.webgl.FLOAT, false, 0, 0);
			viewport.webgl.enableVertexAttribArray(viewport.program.attribute.aVertexWeights);
		}
		
		// Weights Indices
		if(viewport.program.attribute.aVertexWeightIndices !== undefined){
			// Buffer mesh weights
			if(!this.buffers.weightIndices || !this.valid.weightIndices){
				this.buffers.weightIndices = viewport.webgl.createBuffer();             

				// Pass weight indicies to buffer
				viewport.webgl.bindBuffer(viewport.webgl.ARRAY_BUFFER, this.buffers.weightIndices);   
				viewport.webgl.bufferData(viewport.webgl.ARRAY_BUFFER, this.weightIndices, viewport.webgl.STATIC_DRAW);         
				
				this.valid.weightIndices = true;
			}
			viewport.webgl.bindBuffer(viewport.webgl.ARRAY_BUFFER, this.buffers.weightIndices);   
			
			// Tell the vertex shader how to read the array
			viewport.webgl.vertexAttribPointer(viewport.program.attribute.aVertexWeightIndices, 4, viewport.webgl.FLOAT, false, 0, 0);
			viewport.webgl.enableVertexAttribArray(viewport.program.attribute.aVertexWeightIndices);
		}

		// Joint transforms
		if(viewport.program.uniform.uDeformMatrix !== undefined){
			viewport.webgl.uniformMatrix4fv(viewport.program.uniform.uDeformMatrix, false, model.skeleton.getFlatMatrix(entity.transform.mat4));
		}

		// Draw
		viewport.webgl.drawArrays(viewport.webgl.TRIANGLES, 0, this.vertices.length / 3);
	}

	invalidate(vertices = !this.valid.vertices, uvs = !this.valid.uvs, normals = !this.valid.normals){
		this.valid = { // TODO: Make this an array per viewport
			vertices: false,
			uvs: false,
			normals: false,
			weights: false,
			weightIndices: false
		}
	}

	//TODO: Should this be a geometry class?
	addPlane(width, height){
		//TODO: Generalize this concept
		function Float32Concat(first, second){
			var firstLength = first.length,
				result = new Float32Array(firstLength + second.length);
		
			result.set(first);
			result.set(second, firstLength);
		
			return result;
		}

		this.vertices = Float32Concat(this.vertices, [
			0.0,         0.0,          0.0,
			1.0 * width, 0.0,          0.0,
			1.0 * width, 1.0 * height, 0.0,
			0.0,         1.0 * height, 0.0,
			0.0,         0.0,          0.0,
			1.0 * width, 1.0 * height, 0.0 
		]);
		this.uvs = Float32Concat(this.uvs, [
			0.0, 1.0, //  left,    top, 
			1.0, 1.0, // right,    top, 
			1.0, 0.0, // right, bottom, 
			0.0, 0.0, //  left, bottom, 
			0.0, 1.0, //  left,    top, 
			1.0, 0.0  // right, bottom, 
		]);
		this.normals = Float32Concat(this.normals, [
			0.0, 0.0, 1.0,
			0.0, 0.0, 1.0,
			0.0, 0.0, 1.0,
			0.0, 0.0, 1.0,
			0.0, 0.0, 1.0,
			0.0, 0.0, 1.0
		]);

		this.invalidate();

		return this;
	}
}