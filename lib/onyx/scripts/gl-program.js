'use strict';

export default class GLProgram {
	constructor( viewport ) { 
		this.program = viewport.webgl.createProgram();
		this.attribute = {};
		this.uniform = {};
		this.fragmentShader = "";
		this.vertexShader = "";
		
		// If creating the shader program failed, alert
		if (!viewport.webgl.getProgramParameter(this.program, viewport.webgl.LINK_STATUS)) {
			//alert("Unable to initialize the shader program.");
		}
	}
	
	addVertexShader( viewport, shader ){
		var vs = viewport.webgl.createShader(viewport.webgl.VERTEX_SHADER);
		viewport.webgl.shaderSource(vs, shader);
		viewport.webgl.compileShader(vs);
		viewport.webgl.attachShader(this.program, vs); // Maybe do this after, so that you can attach/detach shaders
		
		// If creating the shader program failed, alert
		if (!viewport.webgl.getShaderParameter(vs, viewport.webgl.COMPILE_STATUS)) {
		  alert("An error occurred compiling the shaders: " + viewport.webgl.getShaderInfoLog(vs));
		}
		
		this.vertexShader = shader;
	}
	
	addFragmentShader( viewport, shader ){
		var fs = viewport.webgl.createShader(viewport.webgl.FRAGMENT_SHADER);
		viewport.webgl.shaderSource(fs, shader);
		viewport.webgl.compileShader(fs);
		viewport.webgl.attachShader(this.program, fs);
		
		// If creating the shader program failed, alert
		if (!viewport.webgl.getShaderParameter(fs, viewport.webgl.COMPILE_STATUS)) {
		  alert("An error occurred compiling the shaders: " + viewport.webgl.getShaderInfoLog(fs));
		}
		
		this.fragmentShader = shader;
	}
	
	getAttributesFromShaders( viewport, attrs ){
		for(var i in attrs){
			this.attribute[attrs[i]] = viewport.webgl.getAttribLocation(this.program, attrs[i]);
			if(this.attribute[attrs[i]] < 0) delete this.attribute[attrs[i]];
		}
	}
	
	getUniformLocations( viewport, uniforms ){
		for(var i in uniforms){
			this.uniform[uniforms[i]] = viewport.webgl.getUniformLocation(this.program, uniforms[i]);
			if(this.uniform[uniforms[i]] === null) delete this.uniform[uniforms[i]];
		}
	}
	
	use( viewport ){
		viewport.webgl.useProgram( this.program );
	}
}