'use strict';

import Importer from '../importer';
import Sprite from '../sprite';
import SpriteAnimation from '../sprite-animation';

export default class ImportSpriteFromTexturePacker extends Importer {
    async load(){
        // If JSON, then parse it
        // if(/^[\[|\{](\s|.*|\w)*[\]|\}]$/.test(this.input)){
            let obj = JSON.parse(this.input);
        // }else{
        //     // TODO: Throw error?
        //     return;
        // }

        // console.log(obj);

        let sprite = new Sprite(this.resPath + "/" + obj.meta.image);
        await sprite.loadTextures();
        let name = this.srcFilename.split(".")[0];

        let animation = new SpriteAnimation(name);
        for(let frame of obj.frames){
            animation.addFrame({
                speed: this.options.speed || 15 / 1000, // Default of 15 FPS
                x: frame.frame.x * 1,
                y: frame.frame.y * 1,
                width: frame.frame.w * 1,
                height: frame.frame.h * 1,
                pivotX: frame.pivot.x * 1,
                pivotY: frame.pivot.y * 1,
                materialIndex: 0
            });
        }

        // if(options.sortByFilename === undefined || options.sortByFilename){
            
        // }

        sprite.addAnimation(name, animation);

        if(this.options.defaultAnimation !== undefined){
            if(this.options.autoplay || this.options.autoplay === undefined){
                sprite.playAnimation(this.options.defaultAnimation);
            }else{
                sprite.selectAnimation(this.options.defaultAnimation);
            }
        }

        return sprite;
    }
}