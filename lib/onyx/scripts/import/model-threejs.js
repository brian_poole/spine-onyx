'use strict';

import Importer from '../importer';

import Color from '../color';
import Mesh from '../mesh';
import Model from '../model';
import Texture from '../texture';
import Vertex2 from '../vertex2';
import Vertex3 from '../vertex3';

import MaterialPhong from '../materials/phong';

export default class ImportModelFromThreeJS extends Importer {
    async load(){
        let json = JSON.parse(this.input);
        console.log(json);

        let meshes = [];  

        for(var i = 0; i < json.materials.length; i++){
            meshes.push(new Mesh({
                material: new MaterialPhong()
            }));
        }
        
        var _parsed = this.parseThreeJSModel(json);
        
        console.log(_parsed);

        // Convert faces to verts
        let verts = [];
        for (let i = 0; i < meshes.length; i++) {
            let verts = [];
            let normals = [];
            let vertColors = [];
            for (let face of _parsed.faces) {
                if(face.materialIndex === i){
                    // Vertices
                    verts.push(_parsed.vertices[face.a].x, _parsed.vertices[face.a].y, _parsed.vertices[face.a].z, 
                            _parsed.vertices[face.b].x, _parsed.vertices[face.b].y, _parsed.vertices[face.b].z, 
                            _parsed.vertices[face.c].x, _parsed.vertices[face.c].y, _parsed.vertices[face.c].z);
                    
                    // Normals
                    normals.push(face.vertexNormals[0].x, face.vertexNormals[0].y, face.vertexNormals[0].z,
                                face.vertexNormals[0].x, face.vertexNormals[0].y, face.vertexNormals[0].z,
                                face.vertexNormals[0].x, face.vertexNormals[0].y, face.vertexNormals[0].z);

                    // Textures


                    // Colors
                    vertColors.push.apply(vertColors, face.color.color);
                    vertColors.push.apply(vertColors, face.color.color);
                    vertColors.push.apply(vertColors, face.color.color);
                }
            }
            
            let textureVerts = [];
            if(_parsed.textureCoords[i]){ 
                for (let j = 0; j < _parsed.textureCoords[i].length; j++){
                    var textureUV = _parsed.textureCoords[i][j];
                    if(textureUV) textureVerts.push(textureUV[0].x, textureUV[0].y, 
                                    textureUV[1].x, textureUV[1].y, 
                                    textureUV[2].x, textureUV[2].y);
                }
            }

            // Convert to Float32Arrays (more efficient and required for WebGL)
            meshes[i].vertices = new Float32Array(verts);
            meshes[i].textureVertices = new Float32Array(textureVerts);
            meshes[i].normals = new Float32Array(normals);
        }
        
        let model = new Model({
            meshes: meshes
        });

        console.log(model);

        return model;
    }
    
    // Adapted from # https://github.com/mrdoob/three.js/blob/dev/src/loaders/JSONLoader.js
    parseThreeJSModel(json) {
        var parsed = {
            textureCoords : [],
            vertices : [],
            faces : []
        }

        var Face3 = function(){
            return {
                color: new Color(),
                normal: new Vertex3(),
                vertexNormals: [],
                vertexColors: [],
            }
        }

        function isBitSet(value, position) {
            return value & (1 << position);
        }

        var i, j, fi,

            offset, zLength, colorIndex, normalIndex, uvIndex, materialIndex,

            type,
            isQuad,
            hasMaterial,
            hasFaceVertexUv,
            hasFaceNormal, hasFaceVertexNormal,
            hasFaceColor, hasFaceVertexColor,

            vertex, face, faceA, faceB, hex, normal,

            uvLayer, uv, u, v,

            faces = json.faces,
            vertices = json.vertices,
            normals = json.normals,
            colors = json.colors,

            nUvLayers = 0;

        if (json.uvs !== undefined) {
            // disregard empty arrays
            for (i = 0; i < json.uvs.length; i++) {
                if (json.uvs[i].length) nUvLayers++;
            }

            for (i = 0; i < nUvLayers; i++) {
                parsed.textureCoords[i] = [];
            }
        }

        offset = 0;
        zLength = vertices.length;

        while (offset < zLength) {
            vertex = new Vertex3();

            vertex.x = vertices[offset++];
            vertex.y = vertices[offset++];
            vertex.z = vertices[offset++];

            parsed.vertices.push(vertex);
        }

        offset = 0;
        zLength = faces.length;

        while (offset < zLength) {
            type = faces[offset++];

            isQuad = isBitSet(type, 0);
            hasMaterial = isBitSet(type, 1);
            hasFaceVertexUv = isBitSet(type, 3);
            hasFaceNormal = isBitSet(type, 4);
            hasFaceVertexNormal = isBitSet(type, 5);
            hasFaceColor = isBitSet(type, 6);
            hasFaceVertexColor = isBitSet(type, 7);

            console.log("type", type, "bits", isQuad, hasMaterial, hasFaceVertexUv, hasFaceNormal, hasFaceVertexNormal, hasFaceColor, hasFaceVertexColor);

            if (isQuad) {
                faceA = new Face3();
                faceA.a = faces[offset];
                faceA.b = faces[offset + 1];
                faceA.c = faces[offset + 3];

                faceB = new Face3();
                faceB.a = faces[offset + 1];
                faceB.b = faces[offset + 2];
                faceB.c = faces[offset + 3];

                offset += 4;

                if (hasMaterial) {
                    materialIndex = faces[offset++];
                    faceA.materialIndex = materialIndex;
                    faceB.materialIndex = materialIndex;
                }

                // to get face <=> uv index correspondence

                fi = parsed.faces.length;

                if (hasFaceVertexUv) {
                    for (i = 0; i < nUvLayers; i++) {
                        uvLayer = json.uvs[i];

                        parsed.textureCoords[i][fi] = [];
                        parsed.textureCoords[i][fi + 1] = [];

                        for (j = 0; j < 4; j++) {
                            uvIndex = faces[offset++];

                            u = uvLayer[uvIndex * 2];
                            v = uvLayer[uvIndex * 2 + 1];

                            uv = new Vertex2(u, v);

                            if (j !== 2) parsed.textureCoords[i][fi].push(uv);
                            if (j !== 0) parsed.textureCoords[i][fi + 1].push(uv);
                        }
                    }
                }

                if (hasFaceNormal) {
                    normalIndex = faces[offset++] * 3;

                    faceA.normal.set(
                        normals[normalIndex++],
                        normals[normalIndex++],
                        normals[normalIndex]
                    );

                    faceB.normal.copy(faceA.normal);
                }

                if (hasFaceVertexNormal) {
                    for (i = 0; i < 4; i++) {
                        normalIndex = faces[offset++] * 3;

                        normal = new Vertex3(
                            normals[normalIndex++],
                            normals[normalIndex++],
                            normals[normalIndex]
                        );


                        if (i !== 2) faceA.vertexNormals.push(normal);
                        if (i !== 0) faceB.vertexNormals.push(normal);
                    }
                }


                if (hasFaceColor && colors) {
                    colorIndex = faces[offset++];
                    hex = colors[colorIndex];

                    faceA.color.setHex(hex);
                    faceB.color.setHex(hex);
                }


                if (hasFaceVertexColor && colors) {
                    for (i = 0; i < 4; i++) {

                        colorIndex = faces[offset++];
                        hex = colors[colorIndex];

                        if (i !== 2) faceA.vertexColors.push(new Color(hex));
                        if (i !== 0) faceB.vertexColors.push(new Color(hex));
                    }
                }

                parsed.faces.push(faceA);
                parsed.faces.push(faceB);

            } else {

                face = new Face3();
                face.a = faces[offset++];
                face.b = faces[offset++];
                face.c = faces[offset++];

                if (hasMaterial) {

                    materialIndex = faces[offset++];
                    face.materialIndex = materialIndex;

                }

                // to get face <=> uv index correspondence

                fi = parsed.faces.length;

                if (hasFaceVertexUv) {

                    for (i = 0; i < nUvLayers; i++) {

                        uvLayer = json.uvs[i];

                        parsed.textureCoords[i][fi] = [];

                        for (j = 0; j < 3; j++) {

                            uvIndex = faces[offset++];

                            u = uvLayer[uvIndex * 2];
                            v = uvLayer[uvIndex * 2 + 1];

                            uv = new Vertex2(u, v);

                            parsed.textureCoords[i][fi].push(uv);

                        }

                    }

                }

                if (hasFaceNormal) {

                    normalIndex = faces[offset++] * 3;

                    face.normal.set(
                        normals[normalIndex++],
                        normals[normalIndex++],
                        normals[normalIndex]
                    );

                }

                if (hasFaceVertexNormal) {

                    for (i = 0; i < 3; i++) {

                        normalIndex = faces[offset++] * 3;

                        normal = new Vertex3(
                            normals[normalIndex++],
                            normals[normalIndex++],
                            normals[normalIndex]
                        );

                        face.vertexNormals.push(normal);

                    }

                }


                if (hasFaceColor && colors) {

                    colorIndex = faces[offset++];
                    face.color.parseHex(colors[colorIndex]);

                }


                if (hasFaceVertexColor && colors) {

                    for (i = 0; i < 3; i++) {

                        colorIndex = faces[offset++];
                        face.vertexColors.push(new Color(colors[colorIndex]));

                    }

                }

                parsed.faces.push(face);

            }

        }

        return parsed;
    }
}