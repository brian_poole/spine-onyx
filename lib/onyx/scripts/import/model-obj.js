'use strict';

import Importer from '../importer';

import Mesh from '../mesh';
import Model from '../model';
import Texture from '../texture';

import MaterialPhong from '../materials/phong';

export default class ImportModelFromOBJ extends Importer {
    async load(output){
        let lines = this.input.split("\n");

        let currentMesh = null;
        let meshes = [];
        let verts = [];
        let vertTs = [];
        let vertNs = [];
        let vertPs = [];
        // let faces = [];

        // Parse Lines
        let tLine = [];
        for(let line of lines){
            tLine = line.split(" ");
            
            switch(tLine[0]){
                case "mtllib":
                    let materials = await this.loadMaterialLib(tLine[1]);

                    for(let material of materials){
                        meshes.push(new Mesh({
                            material: material,
                            vertices: [], // Creating standard arrays to make it easier to push to  
                            textureVertices: [],
                            normals: [],
                            // parameters: vertPs,
                            // faces: faces
                        }));
                    }
                    break;
                case "usemtl":
                    currentMesh = this.getMeshByMaterialId(tLine[1], meshes);
                    break;
                case "v":
                    this.parseGeometricVertices(tLine, verts);
                    break;
                case "vt":
                    this.parseTextureCoordinates(tLine, vertTs);
                    break;
                case "vn":
                    this.parseVertexNormals(tLine, vertNs);
                    break;
                case "vp":
                    this.parseParameterSpaceVertices(tLine, vertPs);
                    break;
                case "f":
                    this.parsePolygonalFaces(tLine, verts, vertTs, vertNs, vertPs, currentMesh);
                    break;
                case "o":
                    // Object name
                    break;
                case "g":
                    // Group name
                    break;
                case "s":
                    // Smooth shading
                    break;
            }
        }

        // Convert to Float32Arrays (more efficient and required for WebGL)
        for(let mesh of meshes){
            mesh.vertices = new Float32Array(mesh.vertices);
            mesh.uvs = new Float32Array(mesh.textureVertices);
            mesh.normals = new Float32Array(mesh.normals);

            // let vertCs = [];
            // for(var i = 0; i < mesh.vertices.length * 4; i += 4){
            //     vertCs[i] = i / mesh.vertices.length;
            //     // vertCs[i + 1] = 1 / (i / mesh.vertices.length);
            //     vertCs[i + 1] = 0;
            //     vertCs[i + 2] = 1 / (i / mesh.vertices.length);
            //     // vertCs[i + 2] = 0;
            //     vertCs[i + 3] = 1;
            // }
            // mesh.material.vertexColors = new Float32Array(vertCs);
        }
        
        let model = new Model({
            meshes: meshes
        });

        // console.log(model);

        return model;
    }

    async loadMaterialLib(filename){
        let materials = [];

        let contents = await this.fetchFromURL(this.resPath + "/" + filename);
        let lines = contents.split("\n");

        let currentMaterial = null;

        // Parse Lines
        let tLine = [];
        for(let line of lines){
            tLine = line.split(" ");
            
            switch(tLine[0]){
                case "newmtl":
                    materials.push(new MaterialPhong(tLine[1], {lighting: 1}));

                    currentMaterial = materials[materials.length - 1];
                    break;
                case "illum":
                    currentMaterial.illuminationMode = tLine[1] * 1;
                    break;
                case "ka":
                    currentMaterial.ambientColor.set(tLine[1], tLine[2], tLine[3]);
                    break;
                case "kd":
                    currentMaterial.diffuseColor.set(tLine[1], tLine[2], tLine[3]);
                    break;
                // case "ks":
                //     currentMaterial.specularColor.set(tLine[1], tLine[2], tLine[3]);
                //     // TODO: Read Ns
                //     break;
                case "d":
                    currentMaterial.dissolve = tLine[1] * 1;
                    // TODO: Read Tr
                    break;
                case "map_Ka":
                    currentMaterial.ambientTexture = new Texture(this.resPath + "/" + tLine[tLine.length - 1]);
                    await currentMaterial.ambientTexture.load();
                    break;
                case "map_Kd":
                    currentMaterial.diffuseTexture = new Texture(this.resPath + "/" + tLine[tLine.length - 1]);
                    await currentMaterial.diffuseTexture.load();
                    break;
                case "map_Ks":
                    currentMaterial.specularColorTexture = new Texture(this.resPath + "/" + tLine[tLine.length - 1]);
                    await currentMaterial.specularColorTexture.load();
                    break;
                case "map_Ns":
                    currentMaterial.specularHighlightTexture = new Texture(this.resPath + "/" + tLine[tLine.length - 1]);
                    await currentMaterial.specularHighlightTexture.load();
                    break;
                case "map_d":
                    currentMaterial.alphaTexture = new Texture(this.resPath + "/" + tLine[tLine.length - 1]);
                    await currentMaterial.alphaTexture.load();
                    break;
                case "map_bump":
                case "bump": // synonymous
                    currentMaterial.bumpMapTexture = new Texture(this.resPath + "/" + tLine[tLine.length - 1]);
                    await currentMaterial.bumpMapTexture.load();
                    break;
                case "disp":
                    currentMaterial.displacementTexture = new Texture(this.resPath + "/" + tLine[tLine.length - 1]);
                    await currentMaterial.displacementTexture.load();
                    break;
                case "decal":
                    currentMaterial.decalTexture = new Texture(this.resPath + "/" + tLine[tLine.length - 1]);
                    await currentMaterial.decalTexture.load();
                    break;
            }
        }

        for(let material of materials){
            material.buildProgram();
        }

        return materials;
    }
    
    getMeshByMaterialId(id, meshes){
        for(let mesh of meshes){
            if(mesh.material.name === id) return mesh;
        }

        return null;
    }

    parseGeometricVertices(line, output){
        // # List of geometric vertices, with (x,y,z[,w]) coordinates, w is optional and defaults to 1.0.
        // v 0.123 0.234 0.345 1.0
        // v ...
        // ...

        // Default w to 1.0
        if(line[4] === undefined) line[4] = 1.0;

        // return new Vertex3(line[0], line[1], line[2], line[3]);
        output.push(line[1] * 1, line[2] * 1, line[3] * 1);
    }

    parseTextureCoordinates(line, output){
        // # List of texture coordinates, in (u, v [,w]) coordinates, these will vary between 0 and 1, w is optional and defaults to 0.
        // vt 0.500 1 [0]
        // vt ...
        // ...
        
        if(line[3] === undefined){
            output.push(line[1] * 1, line[2] * 1);
        }else{
            output.push(line[1] * 1, line[2] * 1, line[3] * 1 || 0);
        }
    }

    parseVertexNormals(line, output){
        // # List of vertex normals in (x,y,z) form; normals might not be unit vectors.
        // vn 0.707 0.000 0.707
        // vn ...
        // ...
        
        output.push(line[1] * 1, line[2] * 1, line[3] * 1);
    }

    parseParameterSpaceVertices(line, output){
        // # Parameter space vertices in ( u [,v] [,w] ) form; free form geometry statement ( see below )
        // vp 0.310000 3.210000 2.100000
        // vp ...
        // ...

        output.push(line[1] * 1, line[2] * 1, line[3] * 1);
    }

    parsePolygonalFaces(line, verts, vertTs, vertNs, vertPs, mesh){
        // # Polygonal face element
        // f 1 2 3 (just vertices)
        // f 3/1 4/2 5/3 (vertices/texturecoordinates)
        // f 6/4/1 3/5/3 7/6/5 (vertices/texturecoordinates/normals)
        // f 7//1 8//2 9//3 (vertices//normals)
        // f ...

        let parts = [];
        for(var i = 1; i < line.length; i++){
            parts[i - 1] = line[i].split("/"); // 6/4/1 3/5/3 7/6/5 -> [[6,4,1], [3,5,3], [7,6,5]]
        }

        // let face = {
        if(parts.length === 3){
            mesh.vertices.push(verts[((parts[0][0] * 1 - 1) * 3) + 0], verts[((parts[0][0] * 1 - 1) * 3) + 1], verts[((parts[0][0] * 1 - 1) * 3) + 2],
                                verts[((parts[1][0] * 1 - 1) * 3) + 0], verts[((parts[1][0] * 1 - 1) * 3) + 1], verts[((parts[1][0] * 1 - 1) * 3) + 2],
                                verts[((parts[2][0] * 1 - 1) * 3) + 0], verts[((parts[2][0] * 1 - 1) * 3) + 1], verts[((parts[2][0] * 1 - 1) * 3) + 2]);
            if(parts[0][1] !== "") mesh.textureVertices.push(vertTs[((parts[0][1] * 1 - 1) * 2) + 0], vertTs[((parts[0][1] * 1 - 1) * 2) + 1],
                                vertTs[((parts[1][1] * 1 - 1) * 2) + 0], vertTs[((parts[1][1] * 1 - 1) * 2) + 1],
                                vertTs[((parts[2][1] * 1 - 1) * 2) + 0], vertTs[((parts[2][1] * 1 - 1) * 2) + 1]);
            if(parts[0][2] !== "") mesh.normals.push(vertNs[((parts[0][2] * 1 - 1) * 3) + 0], vertNs[((parts[0][2] * 1 - 1) * 3) + 1], vertNs[((parts[0][2] * 1 - 1) * 3) + 2],
                        vertNs[((parts[1][2] * 1 - 1) * 3) + 0], vertNs[((parts[1][2] * 1 - 1) * 3) + 1], vertNs[((parts[1][2] * 1 - 1) * 3) + 2],
                        vertNs[((parts[2][2] * 1 - 1) * 3) + 0], vertNs[((parts[2][2] * 1 - 1) * 3) + 1], vertNs[((parts[2][2] * 1 - 1) * 3) + 2]);
        }else{
            mesh.vertices.push(verts[((parts[0][0] * 1 - 1) * 3) + 0], verts[((parts[0][0] * 1 - 1) * 3) + 1], verts[((parts[0][0] * 1 - 1) * 3) + 2],
                                verts[((parts[1][0] * 1 - 1) * 3) + 0], verts[((parts[1][0] * 1 - 1) * 3) + 1], verts[((parts[1][0] * 1 - 1) * 3) + 2],
                                verts[((parts[2][0] * 1 - 1) * 3) + 0], verts[((parts[2][0] * 1 - 1) * 3) + 1], verts[((parts[2][0] * 1 - 1) * 3) + 2],
                                verts[((parts[0][0] * 1 - 1) * 3) + 0], verts[((parts[0][0] * 1 - 1) * 3) + 1], verts[((parts[0][0] * 1 - 1) * 3) + 2],
                                verts[((parts[2][0] * 1 - 1) * 3) + 0], verts[((parts[2][0] * 1 - 1) * 3) + 1], verts[((parts[2][0] * 1 - 1) * 3) + 2],
                                verts[((parts[3][0] * 1 - 1) * 3) + 0], verts[((parts[3][0] * 1 - 1) * 3) + 1], verts[((parts[3][0] * 1 - 1) * 3) + 2]);
            if(parts[0][1] !== "") mesh.textureVertices.push(vertTs[((parts[0][1] * 1 - 1) * 2) + 0], vertTs[((parts[0][1] * 1 - 1) * 2) + 1],
                                        vertTs[((parts[1][1] * 1 - 1) * 2) + 0], vertTs[((parts[1][1] * 1 - 1) * 2) + 1],
                                        vertTs[((parts[2][1] * 1 - 1) * 2) + 0], vertTs[((parts[2][1] * 1 - 1) * 2) + 1],
                                        vertTs[((parts[0][1] * 1 - 1) * 2) + 0], vertTs[((parts[0][1] * 1 - 1) * 2) + 1],
                                        vertTs[((parts[2][1] * 1 - 1) * 2) + 0], vertTs[((parts[2][1] * 1 - 1) * 2) + 1],
                                        vertTs[((parts[3][1] * 1 - 1) * 2) + 0], vertTs[((parts[3][1] * 1 - 1) * 2) + 1]);
            if(parts[0][2] !== "") mesh.normals.push(vertNs[((parts[0][2] * 1 - 1) * 3) + 0], vertNs[((parts[0][2] * 1 - 1) * 3) + 1], vertNs[((parts[0][2] * 1 - 1) * 3) + 2],
                                vertNs[((parts[1][2] * 1 - 1) * 3) + 0], vertNs[((parts[1][2] * 1 - 1) * 3) + 1], vertNs[((parts[1][2] * 1 - 1) * 3) + 2],
                                vertNs[((parts[2][2] * 1 - 1) * 3) + 0], vertNs[((parts[2][2] * 1 - 1) * 3) + 1], vertNs[((parts[2][2] * 1 - 1) * 3) + 2],
                                vertNs[((parts[0][2] * 1 - 1) * 3) + 0], vertNs[((parts[0][2] * 1 - 1) * 3) + 1], vertNs[((parts[0][2] * 1 - 1) * 3) + 2],
                                vertNs[((parts[2][2] * 1 - 1) * 3) + 0], vertNs[((parts[2][2] * 1 - 1) * 3) + 1], vertNs[((parts[2][2] * 1 - 1) * 3) + 2],
                                vertNs[((parts[3][2] * 1 - 1) * 3) + 0], vertNs[((parts[3][2] * 1 - 1) * 3) + 1], vertNs[((parts[3][2] * 1 - 1) * 3) + 2]);
        }
            
        // };
    }

}