'use strict';

import Importer from '../importer';
import Sprite from '../sprite';

export default class ImportModelFromThreeJS extends Importer {
	constructor() {
        super(...args);
    }
    
    process(){
        console.log(this.input, this.output);
    }

    loadFromPath(src) {
        ModelCache.load(src).then((model) => {
            this._model = model; // For debug purposes

            this.metadata = model.metadata;
            this.materials = model.materials;
            // this.faces = model.faces;
            // this.uvs = model.uvs;
            // this.vertices = model.vertices;
            // this.normals = model.normals;

            this._parsed = this.parseThreeJSModel(model);

            this.mesh = new Mesh(this._parsed.faces);

            for (let i = 0; i < model.materials.length; i++) {
                let verts = [];
                let vertColors = [];
                for (let face of this._parsed.faces) {
                    if(face.materialIndex === i){
                        verts.push(this._parsed.vertices[face.a].x, this._parsed.vertices[face.a].y, this._parsed.vertices[face.a].z, 
                                this._parsed.vertices[face.b].x, this._parsed.vertices[face.b].y, this._parsed.vertices[face.b].z, 
                                this._parsed.vertices[face.c].x, this._parsed.vertices[face.c].y, this._parsed.vertices[face.c].z);
                        vertColors.push.apply(vertColors, face.color.color);
                        vertColors.push.apply(vertColors, face.color.color);
                        vertColors.push.apply(vertColors, face.color.color);
                    }
                }
                
                let textureVerts = [];
                if(this._parsed.textureCoords[0]){ //[i]){  TODO: Match to material
                    for (let textureUV of this._parsed.textureCoords[0]){ //[i]){  TODO: Match to material
                        textureVerts.push(textureUV[0].x, textureUV[0].y, 
                                        textureUV[1].x, textureUV[1].y, 
                                        textureUV[2].x, textureUV[2].y);
                    }
                }

                let textureSrc = null;
                if(model.materials[i].mapDiffuse){
                    textureSrc = "assets/" + model.materials[i].mapDiffuse;
                }

                this.mesh.vertices[i] = this.mesh.vertices.concat(verts);

                let material = this.mesh.addMaterial(model.materials[i].shading, verts, { uvs : textureVerts, mapDiffuse : textureSrc, vertexColors: vertColors, fogColor: model.materials[i].fogColor, fogDensity: model.materials[i].fogDensity });
            }
        });
    }


    // Adapted from # https://github.com/mrdoob/three.js/blob/dev/src/loaders/JSONLoader.js
    parseThreeJSModel(json, scale = 1.0) {
        var parsed = {
            textureCoords : [],
            vertices : [],
            faces : []
        }

        function isBitSet(value, position) {
            return value & (1 << position);
        }

        var i, j, fi,

            offset, zLength, colorIndex, normalIndex, uvIndex, materialIndex,

            type,
            isQuad,
            hasMaterial,
            hasFaceVertexUv,
            hasFaceNormal, hasFaceVertexNormal,
            hasFaceColor, hasFaceVertexColor,

            vertex, face, faceA, faceB, hex, normal,

            uvLayer, uv, u, v,

            faces = json.faces,
            vertices = json.vertices,
            normals = json.normals,
            colors = json.colors,

            nUvLayers = 0;

        if (json.uvs !== undefined) {
            // disregard empty arrays
            for (i = 0; i < json.uvs.length; i++) {
                if (json.uvs[i].length) nUvLayers++;
            }

            for (i = 0; i < nUvLayers; i++) {
                parsed.textureCoords[i] = [];
            }
        }

        offset = 0;
        zLength = vertices.length;

        while (offset < zLength) {
            vertex = new Vertex3();

            vertex.x = vertices[offset++] * scale;
            vertex.y = vertices[offset++] * scale;
            vertex.z = vertices[offset++] * scale;

            parsed.vertices.push(vertex);
        }

        offset = 0;
        zLength = faces.length;

        while (offset < zLength) {
            type = faces[offset++];

            isQuad = isBitSet(type, 0);
            hasMaterial = isBitSet(type, 1);
            hasFaceVertexUv = isBitSet(type, 3);
            hasFaceNormal = isBitSet(type, 4);
            hasFaceVertexNormal = isBitSet(type, 5);
            hasFaceColor = isBitSet(type, 6);
            hasFaceVertexColor = isBitSet(type, 7);

            // console.log("type", type, "bits", isQuad, hasMaterial, hasFaceVertexUv, hasFaceNormal, hasFaceVertexNormal, hasFaceColor, hasFaceVertexColor);

            if (isQuad) {
                faceA = new Face3();
                faceA.a = faces[offset];
                faceA.b = faces[offset + 1];
                faceA.c = faces[offset + 3];

                faceB = new Face3();
                faceB.a = faces[offset + 1];
                faceB.b = faces[offset + 2];
                faceB.c = faces[offset + 3];

                offset += 4;

                if (hasMaterial) {
                    materialIndex = faces[offset++];
                    faceA.materialIndex = materialIndex;
                    faceB.materialIndex = materialIndex;
                }

                // to get face <=> uv index correspondence

                fi = parsed.faces.length;

                if (hasFaceVertexUv) {
                    for (i = 0; i < nUvLayers; i++) {
                        uvLayer = json.uvs[i];

                        parsed.textureCoords[i][fi] = [];
                        parsed.textureCoords[i][fi + 1] = [];

                        for (j = 0; j < 4; j++) {
                            uvIndex = faces[offset++];

                            u = uvLayer[uvIndex * 2];
                            v = uvLayer[uvIndex * 2 + 1];

                            uv = new Vertex2(u, v);

                            if (j !== 2) parsed.textureCoords[i][fi].push(uv);
                            if (j !== 0) parsed.textureCoords[i][fi + 1].push(uv);
                        }
                    }
                }

                if (hasFaceNormal) {
                    normalIndex = faces[offset++] * 3;

                    faceA.normal.set(
                        normals[normalIndex++],
                        normals[normalIndex++],
                        normals[normalIndex]
                    );

                    faceB.normal.copy(faceA.normal);
                }

                if (hasFaceVertexNormal) {
                    for (i = 0; i < 4; i++) {
                        normalIndex = faces[offset++] * 3;

                        normal = new Vertex3(
                            normals[normalIndex++],
                            normals[normalIndex++],
                            normals[normalIndex]
                        );


                        if (i !== 2) faceA.vertexNormals.push(normal);
                        if (i !== 0) faceB.vertexNormals.push(normal);
                    }
                }


                if (hasFaceColor) {
                    colorIndex = faces[offset++];
                    hex = colors[colorIndex];

                    faceA.color.setHex(hex);
                    faceB.color.setHex(hex);
                }


                if (hasFaceVertexColor) {
                    for (i = 0; i < 4; i++) {

                        colorIndex = faces[offset++];
                        hex = colors[colorIndex];

                        if (i !== 2) faceA.vertexColors.push(new Color(hex));
                        if (i !== 0) faceB.vertexColors.push(new Color(hex));
                    }
                }

                parsed.faces.push(faceA);
                parsed.faces.push(faceB);

            } else {

                face = new Face3();
                face.a = faces[offset++];
                face.b = faces[offset++];
                face.c = faces[offset++];

                if (hasMaterial) {

                    materialIndex = faces[offset++];
                    face.materialIndex = materialIndex;

                }

                // to get face <=> uv index correspondence

                fi = parsed.faces.length;

                if (hasFaceVertexUv) {

                    for (i = 0; i < nUvLayers; i++) {

                        uvLayer = json.uvs[i];

                        parsed.textureCoords[i][fi] = [];

                        for (j = 0; j < 3; j++) {

                            uvIndex = faces[offset++];

                            u = uvLayer[uvIndex * 2];
                            v = uvLayer[uvIndex * 2 + 1];

                            uv = new Vertex2(u, v);

                            parsed.textureCoords[i][fi].push(uv);

                        }

                    }

                }

                if (hasFaceNormal) {

                    normalIndex = faces[offset++] * 3;

                    face.normal.set(
                        normals[normalIndex++],
                        normals[normalIndex++],
                        normals[normalIndex]
                    );

                }

                if (hasFaceVertexNormal) {

                    for (i = 0; i < 3; i++) {

                        normalIndex = faces[offset++] * 3;

                        normal = new Vertex3(
                            normals[normalIndex++],
                            normals[normalIndex++],
                            normals[normalIndex]
                        );

                        face.vertexNormals.push(normal);

                    }

                }


                if (hasFaceColor) {

                    colorIndex = faces[offset++];
                    face.color.parseHex(colors[colorIndex]);

                }


                if (hasFaceVertexColor) {

                    for (i = 0; i < 3; i++) {

                        colorIndex = faces[offset++];
                        face.vertexColors.push(new Color(colors[colorIndex]));

                    }

                }

                parsed.faces.push(face);

            }

        }

        return parsed;
    }
}