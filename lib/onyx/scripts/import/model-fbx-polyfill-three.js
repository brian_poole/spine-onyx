import Bone from '../bone';
import Color from '../color';
import Mesh from '../mesh';
import Model from '../model';
import Skeleton from '../skeleton';
import Texture from '../texture';

import MaterialPhong from '../materials/phong';

var THREE = {
	PropertyBinding: {
		sanitizeNodeName: function(val){ return val }
	},
	// BufferGeometry: function(){
	//     return {
	//         name: "",

	//     }
    // },
    Math: {
        clamp: function ( value, min, max ) {
            
            return Math.max( min, Math.min( max, value ) );
    
		},
		degToRad: function ( degrees ) {
			
			return degrees * Math.PI / 180;
	
		},
	
		radToDeg: function ( radians ) {
	
			return radians * 180 / Math.PI;
	
		},
    },
	Float32BufferAttribute: function(val, itemSize, normalized = true){
		return {
			itemSize: itemSize,
			normalized: normalized,
			array: new Float32Array( val )
		}
    },  
    AnimationClip: function(name, index, tracks){
		return {
			name: name,
			index: index,
			tracks: tracks,
			parseAnimation: function(animationData, bones){

				return {
					animationData: animationData,
					bones: bones
				};
			}
		}
    },
	Bone: function(){
		let bone = new Bone();
		bone.children = bone.children;
		bone.add = function(obj){
			obj.parent = this;
			bone.children.push(obj);
		};
		bone.matrixWorld = bone.transform.mat4;
		bone.matrixWorld.copy = function(val){
			for(var i = 0; i < val.length; i++){
				bone.matrixWorld[i] = val[i];
			}
        }
        bone.euler = new THREE.Euler();
		bone.position = new THREE.Vector3();
		bone.rotation = new THREE.Euler();
        bone.scale = new THREE.Vector3(1,1,1);
        bone.quaternion = new THREE.Quaternion();

		return bone;
	},
	BufferGeometry: function(){
		this.name = "";
		this.attributes = {}

		return {
			attributes: this.attributes,
			addAttribute: (key, val) => {
				this.attributes[key] = val;
			}
		}
	},
	Color: function(){
		return {
			fromArray: function(arr){
				let color = new Color();
				color.set(arr);
				return color;
			}
		}
	},
	Group: function(){
		return {
			isGroup: true,
			euler: new THREE.Euler(),
			position: new THREE.Vector3(),
			rotation: new THREE.Euler(),
			scale: new THREE.Vector3(1,1,1),
			quaternion: new THREE.Quaternion(),
			parent: null,
			add: function(obj){ 
				obj.parent = this;
				this.children.push(obj)
			},
			updateMatrixWorld: function(){},
			children: []
		}
	},
	MeshPhongMaterial: function(){
		return new MaterialPhong();
	},
	MeshLambertMaterial: function(){
		return new MaterialPhong(); //TODO Add Lambert Material
	},
	Object3D: function(){
		//??
		return {
			position: new THREE.Vector3(),
			rotation: new THREE.Vector3(),
            quaternion: new THREE.Quaternion(),
			scale: new THREE.Vector3(),
			parent: null,
			add: function(obj){
				obj.parent = this;
				this.children.push(obj)
			},
			children: []
		}
	},
	Skeleton: function(bones){
		return new Skeleton({children: bones});
	},
	Mesh: function(geometry, material){
		let mesh = new Mesh({
			material: material
		});

		mesh.bind = function(){
			//??
		}

		mesh.position = new THREE.Vector3();
		mesh.rotation = new THREE.Euler();
		mesh.scale = new THREE.Vector3(1,1,1);
        mesh.quaternion = new THREE.Quaternion();

		return mesh;
	},
	SkinnedMesh: function(geometry, material){
		let mesh = new Mesh({
			material: material
		});

		mesh.bind = function(){
			//??
		}
		
		mesh.position = new THREE.Vector3();
		mesh.rotation = new THREE.Euler();
		mesh.scale = new THREE.Vector3(1,1,1);
        mesh.quaternion = new THREE.Quaternion();

		return mesh;
	},
	TextureLoader: function(path){
		this.path = path;

		return {
			load:  (src) => {
				if(this.path){
					var t = new Texture(this.path + "/" + src);
				}else{
					var t = new Texture(src);
				}
				t.load();
				return t;
			},
			setPath: (path) => {
				this.path = path;
			}
		}
	},
	Euler: function (x, y, z, order) {
		return {
			isEuler: true,
			x: x || 0,
			y: y || 0,
			z: z || 0,
			order: order || "XYZ",
			onChangeCallback: function(){},
			set: function( x, y, z, order ) {
				this.x = x;
				this.y = y;
				this.z = z;
				this.order = order || this.order;
		
				this.onChangeCallback();
		
				return this;
			},
			fromArray: function ( array ) {
				
				this.x = array[ 0 ];
				this.y = array[ 1 ];
				this.z = array[ 2 ];
				if ( array[ 3 ] !== undefined ) this.order = array[ 3 ];
		
				this.onChangeCallback();
		
				return this;
		
			},
			setFromVector3: function( v, order ) {
				return this.set( v.x, v.y, v.z, order || this.order );
            },
            setFromQuaternion: function (q, order, update ) {
                
                var matrix = new THREE.Matrix4();
        
				matrix.makeRotationFromQuaternion( q );
	
				return this.setFromRotationMatrix( matrix, order, update );
            },
            setFromRotationMatrix: function ( m, order, update ) {
                var clamp = THREE.Math.clamp;
        
                // assumes the upper 3x3 of m is a pure rotation matrix (i.e, unscaled)
        
                var te = m.elements;
                var m11 = te[ 0 ], m12 = te[ 4 ], m13 = te[ 8 ];
                var m21 = te[ 1 ], m22 = te[ 5 ], m23 = te[ 9 ];
                var m31 = te[ 2 ], m32 = te[ 6 ], m33 = te[ 10 ];
        
                order = order || this.order;
        
                if ( order === 'XYZ' ) {
        
                    this.y = Math.asin( clamp( m13, - 1, 1 ) );
        
                    if ( Math.abs( m13 ) < 0.99999 ) {
        
                        this.x = Math.atan2( - m23, m33 );
                        this.z = Math.atan2( - m12, m11 );
        
                    } else {
        
                        this.x = Math.atan2( m32, m22 );
                        this.z = 0;
        
                    }
        
                } else if ( order === 'YXZ' ) {
        
                    this.x = Math.asin( - clamp( m23, - 1, 1 ) );
        
                    if ( Math.abs( m23 ) < 0.99999 ) {
        
                        this.y = Math.atan2( m13, m33 );
                        this.z = Math.atan2( m21, m22 );
        
                    } else {
        
                        this.y = Math.atan2( - m31, m11 );
                        this.z = 0;
        
                    }
        
                } else if ( order === 'ZXY' ) {
        
                    this.x = Math.asin( clamp( m32, - 1, 1 ) );
        
                    if ( Math.abs( m32 ) < 0.99999 ) {
        
                        this.y = Math.atan2( - m31, m33 );
                        this.z = Math.atan2( - m12, m22 );
        
                    } else {
        
                        this.y = 0;
                        this.z = Math.atan2( m21, m11 );
        
                    }
        
                } else if ( order === 'ZYX' ) {
        
                    this.y = Math.asin( - clamp( m31, - 1, 1 ) );
        
                    if ( Math.abs( m31 ) < 0.99999 ) {
        
                        this.x = Math.atan2( m32, m33 );
                        this.z = Math.atan2( m21, m11 );
        
                    } else {
        
                        this.x = 0;
                        this.z = Math.atan2( - m12, m22 );
        
                    }
        
                } else if ( order === 'YZX' ) {
        
                    this.z = Math.asin( clamp( m21, - 1, 1 ) );
        
                    if ( Math.abs( m21 ) < 0.99999 ) {
        
                        this.x = Math.atan2( - m23, m22 );
                        this.y = Math.atan2( - m31, m11 );
        
                    } else {
        
                        this.x = 0;
                        this.y = Math.atan2( m13, m33 );
        
                    }
        
                } else if ( order === 'XZY' ) {
        
                    this.z = Math.asin( - clamp( m12, - 1, 1 ) );
        
                    if ( Math.abs( m12 ) < 0.99999 ) {
        
                        this.x = Math.atan2( m32, m22 );
                        this.y = Math.atan2( m13, m11 );
        
                    } else {
        
                        this.x = Math.atan2( - m23, m33 );
                        this.y = 0;
        
                    }
        
                } else {
        
                    console.warn( 'THREE.Euler: .setFromRotationMatrix() given unsupported order: ' + order );
        
                }
        
                this.order = order;
        
                if ( update !== false ) this.onChangeCallback();
        
                return this;
        
            },
		}
    },
    Matrix4: function(){
        return {
            elements: [
                1, 0, 0, 0,
                0, 1, 0, 0,
                0, 0, 1, 0,
                0, 0, 0, 1
        
            ],
            fromArray: function ( array, offset ) {
                
                if ( offset === undefined ) offset = 0;
        
                for ( var i = 0; i < 16; i ++ ) {
        
                    this.elements[ i ] = array[ i + offset ];
        
                }
        
                return this;
        
            },
            makeRotationFromQuaternion: function ( q ) {
                
                var te = this.elements;
        
                var x = q.x, y = q.y, z = q.z, w = q.w;
                var x2 = x + x, y2 = y + y, z2 = z + z;
                var xx = x * x2, xy = x * y2, xz = x * z2;
                var yy = y * y2, yz = y * z2, zz = z * z2;
                var wx = w * x2, wy = w * y2, wz = w * z2;
        
                te[ 0 ] = 1 - ( yy + zz );
                te[ 4 ] = xy - wz;
                te[ 8 ] = xz + wy;
        
                te[ 1 ] = xy + wz;
                te[ 5 ] = 1 - ( xx + zz );
                te[ 9 ] = yz - wx;
        
                te[ 2 ] = xz - wy;
                te[ 6 ] = yz + wx;
                te[ 10 ] = 1 - ( xx + yy );
        
                // last column
                te[ 3 ] = 0;
                te[ 7 ] = 0;
                te[ 11 ] = 0;
        
                // bottom row
                te[ 12 ] = 0;
                te[ 13 ] = 0;
                te[ 14 ] = 0;
                te[ 15 ] = 1;
        
                return this;
        
			},
			applyToBufferAttribute: function () {
					var v1 = new THREE.Vector3();
			
					return function applyToBufferAttribute( attribute ) {
			
						for ( var i = 0, l = attribute.count; i < l; i ++ ) {
			
							v1.x = attribute.getX( i );
							v1.y = attribute.getY( i );
							v1.z = attribute.getZ( i );
			
							v1.applyMatrix4( this );
			
							attribute.setXYZ( i, v1.x, v1.y, v1.z );
			
						}
			
						return attribute;
			
					};
			
				}(),
        }
    },
	Quaternion: function (x, y, z, w) {
		return {
			x: x || 0,
			y: y || 0,
			z: z || 0,
			w: ( w !== undefined ) ? w : 1,
			onChangeCallback: function(){},
			set: function( x, y, z, order ) {
				this.x = x;
				this.y = y;
				this.z = z;
				this.w = w;
		
				this.onChangeCallback();
		
				return this;
            },
            toArray: function ( array, offset ) {
                
                if ( array === undefined ) array = [];
                if ( offset === undefined ) offset = 0;
        
                array[ offset ] = this.x;
                array[ offset + 1 ] = this.y;
                array[ offset + 2 ] = this.z;
                array[ offset + 3 ] = this.w;
        
                return array;
        
            },
			setFromEuler: function (euler, update ) {
				if ( ! ( euler && euler.isEuler ) ) {
		
					throw new Error( 'THREE.Quaternion: .setFromEuler() now expects an Euler rotation rather than a Vector3 and order.' );
		
				}
		
				var x = euler.x, y = euler.y, z = euler.z, order = euler.order;
		
				// http://www.mathworks.com/matlabcentral/fileexchange/
				// 	20696-function-to-convert-between-dcm-euler-angles-quaternions-and-euler-vectors/
				//	content/SpinCalc.m
		
				var cos = Math.cos;
				var sin = Math.sin;
		
				var c1 = cos( x / 2 );
				var c2 = cos( y / 2 );
				var c3 = cos( z / 2 );
		
				var s1 = sin( x / 2 );
				var s2 = sin( y / 2 );
				var s3 = sin( z / 2 );
		
				if ( order === 'XYZ' ) {
		
					this.x = s1 * c2 * c3 + c1 * s2 * s3;
					this.y = c1 * s2 * c3 - s1 * c2 * s3;
					this.z = c1 * c2 * s3 + s1 * s2 * c3;
					this.w = c1 * c2 * c3 - s1 * s2 * s3;
		
				} else if ( order === 'YXZ' ) {
		
					this.x = s1 * c2 * c3 + c1 * s2 * s3;
					this.y = c1 * s2 * c3 - s1 * c2 * s3;
					this.z = c1 * c2 * s3 - s1 * s2 * c3;
					this.w = c1 * c2 * c3 + s1 * s2 * s3;
		
				} else if ( order === 'ZXY' ) {
		
					this.x = s1 * c2 * c3 - c1 * s2 * s3;
					this.y = c1 * s2 * c3 + s1 * c2 * s3;
					this.z = c1 * c2 * s3 + s1 * s2 * c3;
					this.w = c1 * c2 * c3 - s1 * s2 * s3;
		
				} else if ( order === 'ZYX' ) {
		
					this.x = s1 * c2 * c3 - c1 * s2 * s3;
					this.y = c1 * s2 * c3 + s1 * c2 * s3;
					this.z = c1 * c2 * s3 - s1 * s2 * c3;
					this.w = c1 * c2 * c3 + s1 * s2 * s3;
		
				} else if ( order === 'YZX' ) {
		
					this.x = s1 * c2 * c3 + c1 * s2 * s3;
					this.y = c1 * s2 * c3 + s1 * c2 * s3;
					this.z = c1 * c2 * s3 - s1 * s2 * c3;
					this.w = c1 * c2 * c3 - s1 * s2 * s3;
		
				} else if ( order === 'XZY' ) {
		
					this.x = s1 * c2 * c3 - c1 * s2 * s3;
					this.y = c1 * s2 * c3 - s1 * c2 * s3;
					this.z = c1 * c2 * s3 + s1 * s2 * c3;
					this.w = c1 * c2 * c3 + s1 * s2 * s3;
		
				}
		
				if ( update !== false ) this.onChangeCallback();
		
				return this;
			},
			multiply: function ( q, p ) {
				
				if ( p !== undefined ) {
		
					console.warn( 'THREE.Quaternion: .multiply() now only accepts one argument. Use .multiplyQuaternions( a, b ) instead.' );
					return this.multiplyQuaternions( q, p );
		
				}
		
				return this.multiplyQuaternions( this, q );
		
			},
			premultiply: function ( q ) {
				
				return this.multiplyQuaternions( q, this );
		
			},
			multiplyQuaternions: function ( a, b ) {
				
				// from http://www.euclideanspace.com/maths/algebra/realNormedAlgebra/quaternions/code/index.htm
		
				var qax = a.x, qay = a.y, qaz = a.z, qaw = a.w;
				var qbx = b.x, qby = b.y, qbz = b.z, qbw = b.w;
		
				this.x = qax * qbw + qaw * qbx + qay * qbz - qaz * qby;
				this.y = qay * qbw + qaw * qby + qaz * qbx - qax * qbz;
				this.z = qaz * qbw + qaw * qbz + qax * qby - qay * qbx;
				this.w = qaw * qbw - qax * qbx - qay * qby - qaz * qbz;
		
				this.onChangeCallback();
		
				return this;
		
			},
		}
	},
	Vector2: function (x, y) {
		this.x = x || 0;
		this.y = y || 0;

		return {
			x: x,
			y: y,
			fromArray: (array, offset) => {
				if (offset === undefined) offset = 0;

				this.x = array[offset];
				this.y = array[offset + 1];

				return this;
			},
			toArray: (array, offset) => {
				if (array === undefined) array = [];
				if (offset === undefined) offset = 0;

				array[offset] = this.x;
				array[offset + 1] = this.y;

				return array;
			}
		}
	},
	Vector3: function (x, y, z) {
		this.x = x || 0;
		this.y = y || 0;
		this.z = z || 0;

		this.fromArray = (array, offset) => {
			if (offset === undefined) offset = 0;

			this.x = array[offset];
			this.y = array[offset + 1];
			this.z = array[offset + 2];

			return this;
		}

		this.toArray = (array, offset) => {
			if (array === undefined) array = [];
			if (offset === undefined) offset = 0;

			array[offset] = this.x;
			array[offset + 1] = this.y;
			array[offset + 2] = this.z;

			return array;
		}

		this.multiplyScalar = (scale) => {
			this.x *= scale;
			this.y *= scale;
			this.z *= scale;

			return this;
		}

		return this;
	},
	Vector4: function (x, y, z, w) {
		this.x = x || 0;
		this.y = y || 0;
		this.z = z || 0;
		this.w = w || 0;

		return {
			x: x,
			y: y,
			z: z,
			w: w,
			fromArray: (array, offset) => {
				if (offset === undefined) offset = 0;

				this.x = array[offset];
				this.y = array[offset + 1];
				this.z = array[offset + 2];
				this.w = array[offset + 3];

				return this;
			},
			toArray: (array, offset) => {
				if (array === undefined) array = [];
				if (offset === undefined) offset = 0;

				array[offset] = this.x;
				array[offset + 1] = this.y;
				array[offset + 2] = this.z;
				array[offset + 3] = this.w;

				return array;
			}
		}
	},
	KeyFrameTrack: function(name, times, values, interpolation, modelID){
		if ( name === undefined ) throw new Error( 'THREE.KeyframeTrack: track name is undefined' );
		if ( times === undefined || times.length === 0 ) throw new Error( 'THREE.KeyframeTrack: no keyframes in track named ' + name );
	
		// this.times = AnimationUtils.convertArray( times, this.TimeBufferType );
		// this.values = AnimationUtils.convertArray( values, this.ValueBufferType );
	
		// this.setInterpolation( interpolation || this.DefaultInterpolation );
	
		// this.validate();
		// this.optimize();
		
		return {
			name: name,
			modelID: modelID,
			times: times,
			values: values,
			interpolation: interpolation
		}
	},
	VectorKeyframeTrack: function(name, times, values, interpolation, modelID){
		var track = new THREE.KeyFrameTrack(name, times, values, interpolation, modelID);

		track.ValueTypeName = 'vector';
		return track;
	},
	QuaternionKeyframeTrack: function(name, times, values, interpolation, modelID, euler, preRotations){
		var track = new THREE.KeyFrameTrack(name, times, values, interpolation, modelID);

		track.euler = euler;
		track.preRotations = preRotations;
		track.ValueTypeName = 'quaternion';
		return track;
	}
}

export default THREE;