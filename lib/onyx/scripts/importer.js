'use strict';

let importCache = {};

export default class Importer {
	constructor(options = {}) {
        this.options = options;
        
        this.resPath = options.resPath;

        this.importCache = importCache;

        // if(input){
        //     return new Promise((resolve, reject) => {
        //         resolve(this.load());
        //     });
        // }else if(url){
            return this.loadFromURL(options.src);
        // }
    }
    
    async load(){

    }

    loadFromURL(src){
        if(!src) return false;

        this.src = src;
        this.srcPath = src.substr(0, src.lastIndexOf("/"));
        if(this.resPath === undefined) this.resPath = this.srcPath;
        this.srcFilename = src.split("/").pop();

        if(this.importCache[src]){
            this.input = this.importCache[src].input;
            return this.load();
        }else{
            return this.fetchFromURL(src).then((response) => {
                this.input = response;
    
                this.importCache[src] = {
                    input: response
                };
    
                return this.load();
            });
        }
    }

    // Fetch
    async fetchFromURL(url){
        return fetch(url, {
            "Content-Type": "text/html"
        }).then((response) => {
            // Read response
            if (!response.ok) {
                throw Error(response.statusText, response.url);
            }
            return response.text();
        }).then(
            // Return contents
            (contents) => contents
        ).catch((e) => {
            console.error(`Error fetching template '${url}'.`, e);
        });
    }
}