'use strict';

const FACE = [
	-1.0,  1.0, 0.0,
	 1.0,  1.0, 0.0,
	 1.0, -1.0, 0.0,
	-1.0, -1.0, 0.0,
	-1.0,  1.0, 0.0,
	 1.0, -1.0, 0.0 
];
            
const FACE_UVS = [
	0.0, 1.0, //  left,    top, 
	1.0, 1.0, // right,    top, 
	1.0, 0.0, // right, bottom, 
	0.0, 0.0, //  left, bottom, 
	0.0, 1.0, //  left,    top, 
	1.0, 0.0  // right, bottom, 
];

const FACE_NORMALS = [
	0.0, 0.0, 1.0,
	0.0, 0.0, 1.0,
	0.0, 0.0, 1.0,
	0.0, 0.0, 1.0,
	0.0, 0.0, 1.0,
	0.0, 0.0, 1.0
];

// Variable Qualifiers

// Qualifiers give a special meaning to the variable. The following qualifiers are available:
//		const – The declaration is of a compile time constant.
//		attribute – Global variables that may change per vertex, that are passed from the OpenGL application to vertex shaders. This qualifier can only be used in vertex shaders. For the shader this is a read-only variable. See Attribute section.
//		uniform – Global variables that may change per primitive [...], that are passed from the OpenGL application to the shaders. This qualifier can be used in both vertex and fragment shaders. For the shaders this is a read-only variable. See Uniform section.
//		varying – used for interpolated data between a vertex shader and a fragment shader. Available for writing in the vertex shader, and read-only in a fragment shader. See Varying section.

// The vertex level shader. This is typically the position on screen.
const VERTEX_SHADER = `
    precision mediump float;

    attribute vec3 aVertexPosition;
    attribute vec2 aTextureCoord;

    uniform float iTime;

    varying highp float vTime;
    varying highp vec2 vTextureCoord;

	void main() {
        vTextureCoord = aTextureCoord;

        vTime = iTime;

        gl_Position = vec4(aVertexPosition, 1.0);
	}
`;

// The fragment (or "pixel") level shader.
const FRAG_SHADER = `
    precision mediump float;

    #define TAU 6.28318530718
    #define MAX_ITER 4

    varying highp float vTime;
    varying highp vec2 vTextureCoord;

    void main(void) {
        gl_FragColor = vec4(1.0, 0.0, 1.0, 1.0);
    }
`;

export default class FrameBufferTexture2D {
	constructor(name = "", {width = 256, height = 256, scale = 1} = {}) {
        this.shaderAttributes = ["aVertexPosition", "aTextureCoord"];
        this.shaderUniforms = ["iTime"];

        this.width = width;
        this.height = height;
        this.scale = scale;
        this.timeOffset = 0;

        this.wrap = {
            s: "REPEAT",
            t: "REPEAT"
        }
        
        this.buildProgram(); // TODO: Where should "autobuild" this live?
        
        this.buffers = {};
        this.valid = {};
	}

	render(dt, scene, viewport){
        let gl = viewport.webgl;

        if(!this.program){
			this.program = viewport.createProgram(this);
		}
        viewport.useProgram(this.program);
        
        // Create the "texture"
        if(!this.texture){
            this.texture = this.createTexture(gl);
        }
        gl.bindTexture(gl.TEXTURE_2D, this.texture);

        viewport._currentTexture[0] = this;

        // Create and bind the framebuffer
        if(!this.fb){
            this.fb = gl.createFramebuffer();
            this.fb.width = this.width;
            this.fb.height = this.height;
        }

        gl.bindFramebuffer(gl.FRAMEBUFFER, this.fb);
        
        // attach the texture as the first color attachment
        const attachmentPoint = gl.COLOR_ATTACHMENT0;
        gl.framebufferTexture2D(gl.FRAMEBUFFER, attachmentPoint, gl.TEXTURE_2D, this.texture, 0); // 0 = level

        // Set the time
        if(viewport.program.uniform.iTime !== undefined){
            viewport.webgl.uniform1f(viewport.program.uniform.iTime, (((scene.clock.stats.currentGameTime) / 100000) + this.timeOffset) / 1000); // Divide by 1000 to help with intigers on lowp
        }
        
        // Buffer mesh verts
        if(viewport.program.attribute.aVertexPosition !== undefined){
            if(!this.buffers.vertices || !this.valid.vertices){
                this.buffers.vertices = viewport.webgl.createBuffer();        

                // Pass vertices to buffer
                // TODO: Do this once per vertices being updated (but bind on every frame)
                viewport.webgl.bindBuffer(viewport.webgl.ARRAY_BUFFER, this.buffers.vertices);   
                viewport.webgl.bufferData(viewport.webgl.ARRAY_BUFFER, new Float32Array(FACE), viewport.webgl.STATIC_DRAW); // TODO: Allow mesh to define type?
                
                this.valid.vertices = true;
            }
            viewport.webgl.bindBuffer(viewport.webgl.ARRAY_BUFFER, this.buffers.vertices);   

            // Tell the vertex shader how to read the array
            viewport.webgl.vertexAttribPointer(viewport.program.attribute.aVertexPosition, 3, viewport.webgl.FLOAT, false, 0, 0);
            viewport.webgl.enableVertexAttribArray(viewport.program.attribute.aVertexPosition);
        }

        // UVs
		if(viewport.program.attribute.aTextureCoord !== undefined){
			// Buffer mesh texture coordinates
			if(!this.buffers.uvs || !this.valid.uvs){
				this.buffers.uvs = viewport.webgl.createBuffer();             

				// Pass texture coordinates to buffer
				viewport.webgl.bindBuffer(viewport.webgl.ARRAY_BUFFER, this.buffers.uvs);   
				viewport.webgl.bufferData(viewport.webgl.ARRAY_BUFFER, new Float32Array(FACE_UVS), viewport.webgl.STATIC_DRAW);        
				
				this.valid.uvs = true; 
			}
			viewport.webgl.bindBuffer(viewport.webgl.ARRAY_BUFFER, this.buffers.uvs);   
			
			// Tell the vertex shader how to read the array
			viewport.webgl.vertexAttribPointer(viewport.program.attribute.aTextureCoord, 2, viewport.webgl.FLOAT, false, 0, 0);
			viewport.webgl.enableVertexAttribArray(viewport.program.attribute.aTextureCoord);
		}
        
        gl.clear(gl.COLOR_BUFFER_BIT);
		
        // Draw
        viewport.webgl.drawArrays(viewport.webgl.TRIANGLES, 0, FACE.length / 3);
        
        // Unbind
        gl.bindFramebuffer(gl.FRAMEBUFFER, null);
    }

    buildProgram(){
		this.vertexShader = VERTEX_SHADER;
        this.fragmentShader = FRAG_SHADER;
    }

    createTexture(gl){
        // create to render to
        const targetTextureWidth = this.width;
        const targetTextureHeight = this.height;
        let targetTexture = gl.createTexture();
        gl.bindTexture(gl.TEXTURE_2D, targetTexture);

        // define size and format of level 0
        const level = 0;
        const internalFormat = gl.RGBA;
        const border = 0;
        const format = gl.RGBA;
        const type = gl.UNSIGNED_BYTE;
        const data = null; // Just tell WebGL to allocate the texture
        gl.texImage2D(gl.TEXTURE_2D, level, internalFormat,
                        targetTextureWidth, targetTextureHeight, border,
                        format, type, data);
        
        // set the filtering so we don't need mips
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);

        return targetTexture;
    }

    setAsTexture(viewport, textureNumber, uniformPosition = null){
        viewport.webgl.activeTexture(viewport.webgl["TEXTURE"+textureNumber]);
		viewport.webgl.bindTexture(viewport.webgl.TEXTURE_2D, this.texture);

		// Allow non-square textures
		// viewport.webgl.texParameteri(viewport.webgl.TEXTURE_2D, viewport.webgl.TEXTURE_WRAP_S, viewport.webgl.CLAMP_TO_EDGE);
		// viewport.webgl.texParameteri(viewport.webgl.TEXTURE_2D, viewport.webgl.TEXTURE_WRAP_T, viewport.webgl.CLAMP_TO_EDGE);

		// viewport.webgl.texParameteri(viewport.webgl.TEXTURE_2D, viewport.webgl.TEXTURE_MAG_FILTER, viewport.webgl[this.magFilter]);
        // viewport.webgl.texParameteri(viewport.webgl.TEXTURE_2D, viewport.webgl.TEXTURE_MIN_FILTER, viewport.webgl[this.minFilter]);
        
        
        // set the filtering so we don't need mips
        viewport.webgl.texParameteri(viewport.webgl.TEXTURE_2D, viewport.webgl.TEXTURE_MAG_FILTER, viewport.webgl.LINEAR);
        viewport.webgl.texParameteri(viewport.webgl.TEXTURE_2D, viewport.webgl.TEXTURE_MIN_FILTER, viewport.webgl.LINEAR);

        viewport.webgl.texParameteri(viewport.webgl.TEXTURE_2D, viewport.webgl.TEXTURE_WRAP_S, viewport.webgl[this.wrap.s]);
        viewport.webgl.texParameteri(viewport.webgl.TEXTURE_2D, viewport.webgl.TEXTURE_WRAP_T, viewport.webgl[this.wrap.t]);

        if(uniformPosition){
			viewport.webgl.uniform1i(uniformPosition, textureNumber);
		}
		
		viewport._currentTexture[textureNumber] = this;
    }
}