'use strict';

import { mat4 } from '../lib/gl-matrix-master/src/gl-matrix';

import Component from './component';
import Transform from './transform';
// import Skeleton from './skeleton';

const tMatrix = mat4.create();

// REFERENCE: Model or Mesh? https://books.google.com/books?id=0bUJAgAAQBAJ&pg=PA38&lpg=PA38&dq=model+or+mesh&source=bl&ots=YHaPzACkgG&sig=ZGRp7qyCQbVlqZECbm0Hx-oZTYc&hl=en&sa=X&ved=0ahUKEwjNsYK59-nWAhUN8YMKHZ5UAoc4ChDoAQg8MAA#v=onepage&q&f=false

export default class Model extends Component {
    constructor({transform = new Transform(), materials = [], meshes = [], bones = [], skeleton = null, skeletalAnimations = [], animationClips = []} = {}) {
		super();
		
		this.transform = transform;

		this.meshes = meshes;
		
		this.animationClips = animationClips;
        this.bones = bones;
        this.skeleton = skeleton;
        this.skeletalAnimations = skeletalAnimations;
	}
	
	update(dt, scene, entity){
		if(this.skeleton) this.skeleton.update(dt);
		
		for(var i = 0; i < this.animationClips.length; i++){
			this.animationClips[i].update(dt);
		}
	}

    render(dt, scene, viewport, camera, entity) {
        // TODO: [Optimization] Keep track of the number of meshes so it doesn't have to grab the length every time
		for(var i = 0; i < this.meshes.length; i++){
			this.meshes[i].render(dt, scene, viewport, camera, entity, this);
		}
    }

    renderPick(viewport, camera, entity, entityId) {
        // TODO
        return;

		if(!this.vertices.length) return;

		// TODO, FIX
		for(var i = 0; i < this.materials.length; i++){
			if(!this.materials[i].vertices.length) continue;

			let vertColors = [];
			for(var j = 0; j < this.vertices[i].length; j++){
				vertColors.push((entityId + 1) / 256); //ID?
				vertColors.push((entityId + 1) / 256);
				vertColors.push((entityId + 1) / 256);
				vertColors.push(1);
			}

			this.materialPick.vertices = this.materials[i].vertices;
			this.materialPick.vertexColors = new Float32Array(vertColors);

			this.materialPick.render(viewport, camera, entity, this, entityId, offset);
		}
    }

    calcParentMatrix(cMatrix = tMatrix){ //TODO: Confusing named function 
        if(!this.parent){
            mat4.copy(cMatrix, this.transform.mat4);
        }else{
            this.parent.calcParentMatrix(cMatrix);
            mat4.multiply(cMatrix, cMatrix, this.transform.mat4);
        }
    }
}