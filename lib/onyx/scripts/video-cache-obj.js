'use strict';

import ResourceCache from './resource-cache';

export default new ResourceCache((src) => {
	let video = document.createElement('video');
	let promise = new Promise((resolve, reject) => {
		video.addEventListener("canplaythrough", () => {
			console.info('video loaded', video.src, video.height); 
		}, true);
		video.addEventListener("canplay", () => {
			console.info('video loaded', video.src, video.height); 
		}, true);
		video.addEventListener("ended", () => {
			console.info('video done', video.src, video.height); 
		}, true);
		video.onerror = ((e) => {
			console.error(e);
			reject(`Error loading video file: ${e.message}`);
		});

		resolve(video);
	});
	video.preload = "auto";
	video.setAttribute("playsinline", "");
	video.setAttribute("webkit-playsinline", "");
	video.src = src;

	return promise;
});