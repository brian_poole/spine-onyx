'use strict';

import Transform from './transform';

export default class Entity {
    constructor(id = null, ...components){
        // Extend _priv vars
        if(!this._priv){
            this._priv = {
                events: []
            };
        }

        if(!this._priv.events) this._priv.events = [];
		
		this.id = id;
        
        this.enabled = true;
        this.visible = true;
		this.opacity = 1.0;
        this.pointerEvents = true;

        this.parentScene = null;

		this.components = components;
		this.componentMap = new Map();

		this.transform = new Transform();
	}

	/**
	 * Called on every Scene update
	 */
	update(dt, scene) {
        for(var i = 0; i < this.components.length; i++){
            if(this.components[i].enabled){
                this.components[i].update(dt, scene, this);
            }
		}
	}

	/**
	 * Called on every Scene render
	 */
	render(dt, scene, viewport, camera) {
        for(var i = 0; i < this.components.length; i++){
            if(this.components[i].visible){
                this.components[i].render(dt, scene, viewport, camera, this );
            }
		}
	}

	/**
	 * Add a new component
	 */
	addComponent( component ) {
		this.components.push( component );
		this.componentMap.set( component.constructor, component );
    }
    
    /**
	 * Add a new component
	 */
	removeComponent( component ) {
        if(this.components.indexOf(component) <= -1) return;
        
		this.components.splice( this.components.indexOf(component), 1 );
	}

	/**
	 * Called on every Scene render pick
	 */
	renderPick( viewport, camera, entityId ) {
        if(!this.visible || !this.pointerEvents) return;
        
		for(var i = 0; i < this.components.length; i++){
            if(this.components[i].visible){
                if(this.components[i].renderPick) this.components[i].renderPick( viewport, camera, this, entityId );
            }
		}
	}

    registerHandler(...args){
        // keep track of eventhandlers so we can remove them on destroy
        // this._priv.events.push();
    }

    unregisterHandler(){
        
    }

    onload(){
        for(let event of this._priv.events){
            this.registerHandler(event);
        }
    }

    onunload(){
        for(let event of this._priv.events){
            this.unregisterHandler(event);
        }
    }

    /**
	 * Called every time the object is added to the scene
	 */
    onSpawn(scene){
        this.parentScene = scene;
    }

	/**
	 * Called every time the object is removed from the scene
	 */
	onDestroy( scene ) {
        
	}

    // TODO
    emit(id, ...args){
        for(let event of this._priv.events){
            if(event.event == id){
                event.target[event.property].apply(this, args);
            }
        }
    }
}