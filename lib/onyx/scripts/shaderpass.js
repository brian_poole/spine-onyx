'use strict';

import GLProgram from './gl-program';

export default class ShaderPass {
	constructor() {
		this.shaderAttributes = [];
		this.shaderUniformLocations = [];

		this.vertexShader = null;
		this.fragmentShader = null;
	}

	render( mesh, entity, camera, viewport ) {
		//TODO: We should compile a shader cache, so that way we aren't unnecessarily swapping shaders
		if(!this.program){
			this.initShaders(viewport);
		}
		viewport.useProgram(this.program);
		
    
	}

	initShaders( viewport ) {
		// Create a new program
		this.program = new GLProgram(viewport);
		
		// Compile shaders
		this.program.addVertexShader(viewport, this.vertexShader);
		this.program.addFragmentShader(viewport, this.fragmentShader);
		
		// Links the compiled shaders to the program
		viewport.webgl.linkProgram(this.program.program);	
		
		// Get attributes from shaders
        this.program.getAttributesFromShaders(viewport, this.shaderAttributes);
		this.program.getUniformLocations(viewport, this.shaderUniformLocations);
	}
}