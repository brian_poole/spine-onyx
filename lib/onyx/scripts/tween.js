'use strict';

import Component from './component';

export default class Tween extends Component {
	constructor() {
		super();
		this.currentTime = 0;
        this.percentComplete = 1;
        this.duration = 0;
        this.direction = 1;
		this.mode = "linear";
        this.propParent = null;
		this.prop = "";
        this.from = 0;
        this.to = 0;
		
		this.callback = null;
	}
	
	update(dt, scene, entity) {
		if(!this.enabled || this.percentComplete === 1) return;
		
		this.currentTime += dt * this.direction;
        this.percentComplete = Math.min(this.currentTime / this.duration, 1);

		switch(this.mode){
			case "linear":
				// a + ((b-a) * %)
                this.propParent[this.prop] = this.from + ((this.to - this.from) * this.percentComplete);
				break;
			case "easeIn":
				// function(a,b,percent) { return a + (b-a)*((-Math.cos(percent*Math.PI)/2) + 0.5);        }
                this.propParent[this.prop] = this.from + (this.to-this.from)*((-Math.cos(this.percentComplete*Math.PI)/2) + 0.5);
				break;
		}

        if(this.percentComplete === 1 && this.callback) this.callback();
	}

    linear( propParent, prop, to, duration, callback ) {
		this.duration = duration;
        this.propParent = propParent;
        this.prop = prop;
        this.from = propParent[prop];
		this.to = to;
        this.callback = callback;

		this.mode = "linear";
		this.currentTime = 0;
		this.percentComplete = 0;
    }
	
	easeIn( propParent, prop, to, duration, callback ) {
		this.duration = duration;
        this.propParent = propParent;
        this.prop = prop;
        this.from = propParent[prop];
		this.to = to;
        this.callback = callback;

		this.mode = "easeIn";
		this.currentTime = 0;
		this.percentComplete = 0;
	}
    
}