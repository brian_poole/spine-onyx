// Credit - Shannon Poole 2017
// https://gist.github.com/shannon/8a587958dfdf29a9d9393d5ed95fc2a1

const http = require('http');
const Koa = require('koa');
const serveFiles = require('koa-static');
const mount = require('koa-mount');
const path = require('path');
const fs = require('fs-extra');
const minimatch = require('minimatch');
const util = require('util');

const transformFile = require('babel-core').transformFile;

const statFile = util.promisify(fs.stat).bind(fs);
const transform = util.promisify(transformFile);

async function needsTranspile(srcPath, cachePath) {
	const srcStat = await statFile(srcPath).catch(() => { });
	const cacheStat = srcStat && await statFile(cachePath).catch(() => { });
	return srcStat && (!cacheStat || (srcStat.mtime > cacheStat.mtime));
}

function transpileMiddleware(pattern) {
	return async (ctx, next) => {
		if (minimatch(ctx.path, pattern)) {
			const srcPath = path.join(process.cwd(), ctx.path);
			const cachePath = path.join(process.cwd(), '.transpiled', ctx.path);

			if (await needsTranspile(srcPath, cachePath)) {
				const { code, map } = await transform(srcPath, {
					// Swap out your own preset or plugins here
					presets: [],
					plugins: [],
					sourceMaps: true,
					compact: false,
				});

				/* eslint-disable no-useless-concat */
				/* eslint-reason sourceMappingURL gets munged by babel istanbul */
				await fs.outputFile(cachePath, `${code}\r//# sourceMappingURL=` + `${path.basename(ctx.path)}.map`);
				await fs.outputFile(`${cachePath}.map`, JSON.stringify(map));
			}
		}

		await next();
	};
}

class Server {
	constructor({
    port = 4200,
		maxage = 1000 * 60 * 60 * 24,
		gzip = true
  } = {}) {
		this.port = port;
		this.maxage = maxage;
		this.gzip = gzip;

		this.app = new Koa();
		this.app.use(transpileMiddleware('/**/*.+(js|mjs)'));

		this.app.use(serveFiles('./.transpiled', { maxage, gzip, extensions: ['js', 'mjs'] }));
		this.app.use(serveFiles('./', { maxage, gzip, extensions: ['js', 'mjs'] }));

		this.server = http.createServer(this.app.callback());
		this.server.keepAliveTimeout = 50;
	}

	get address() {
		return this.server.address();
	}

	/**
	 * Start the server. Resolves when server is active and listening.
	 */
	async start() {
		await new Promise((resolve, reject) => {
			this.server.listen(this.port, err => (err ? reject(err) : resolve()));
		});
		console.info(`Server listening on port ${this.server.address().port}`);
	}

	/**
	 * Stop the server. Resolves when all connections are closed.
	 */
	async stop() {
		console.info('Stopping server');
		await new Promise((resolve, reject) => {
			this.server.close(err => (err ? reject(err) : resolve()));
		});
	}
}

module.exports = Server;