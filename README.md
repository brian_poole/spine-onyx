#Spine-Onyx

A Spine Animation module for importing and using Spine objects with the Onyx Engine.

###Usage

#### Using the module

The module is located in this projects `/module/` folder and is ES6 module compatible. Simply import as so:

```
import ImportSpine from 'path/to/import-spine.js';
```

#### Importing

Use the `ImportSpine` object to asynchronously fetch and parse the Spine component. This will return a Spine component to be used with any Onyx entity.

```
new ImportSpine({src: "res/raptor-pro.json", atlas: "res/raptor.atlas"}).then((spineModel) => {
    // Create a new entity to add the Spine component to
    let newCharacter = new Onyx.Entity("My Spine Character", spineModel);
    
    // Add the new entity to the scene
    this.add(newCharacter);
});
```

#### Setting the animation

Once the Spine character is loaded, any [official spine-ts runtime functions and properties](https://github.com/EsotericSoftware/spine-runtimes/tree/3.6/spine-ts) (or more specifically, [this](https://github.com/EsotericSoftware/spine-runtimes/blob/3.6/spine-ts/core/src/AnimationState.ts)) are able to be used. For example:
```
let spineModel = newCharacter.components[0];
spineModel.state.setAnimation(0, "walk", true);
```

###Running the example

All example development is done in the `/app/` folder. Running the following gulp script will transpile the game from ES6 to ES5, into the `/dist/` folder:

```
$ npm run gulp serve
```

For convenience, a simple localhost on port 8080 will start. Use the environment variable `PORT` to change the port used.


###Distribution

After running locally, the `/dist/` folder is ready for standalone distribution

Alternatively, to just build the `/dist/` folder without serving:
```
$ npm run gulp
```